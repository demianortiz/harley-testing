<?php
App::import('Vendor', 'simplexlsx');
App::import('Vendor', 'PHPExcel');
App::import('Vendor', 'PHPExcel/Writer/Excel2007');
App::uses('CakeEmail', 'Network/Email');
class AdminController extends AppController {

  public function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allow('login');
      $this->layout = 'web';
      $this->loadModel('User');
      $this->loadModel('Group');
      $this->loadModel('Groups_user');
      $this->loadModel('Audit_type');
      $this->loadModel('Audit_answer');
      $this->loadModel('Auditoria');
      $profile = $this->Auth->user();
      $this->set('profile', $profile);
  }

  public function index() {
    $dealers = $ids = array();
    $auditss = $this->get_audits('modified');

    foreach($auditss as $audit) {
        if(!in_array($audit['Dealer']['id'],$ids)) {
            $dealers[] = array('id' => $audit['Dealer']['id'], 'name' => $audit['Dealer']['name']);
        }
        $ids[] = $audit['Dealer']['id'];
    }

    $this->set('validate_audits', $auditss);
    $this->set('dealers', $dealers);


    $audit_types = $this->get_audit_type();
    $this->set('audit_types', $audit_types);
  }

  /************************* Login/Logout Actions **************/

  public function login() {
      if ($this->request->is('post')) {
          if ($this->Auth->login()) {
              $this->redirect($this->Auth->redirect());
          } else {
              $this->Session->setFlash(__('Invalid username or password, try again'));
          }
      }
  }

  public function logout() {
      $this->redirect($this->Auth->logout());
  }

  /************************** TOOLS *****************************/

  public function gen_pass(){
    $this->autoRender  = FALSE;
    $string = '';
    $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    for ($i = 0; $i < 5; $i++) {
        $string .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $string;
  }

  public function add_group(){
    if($this->request->is('post')){
      $data['Group']['parent_id'] = $this->request->data['last_selected'];
      $data['Group']['name'] = $this->request->data['name'];
      $this->Group->save($data);
    }      
    $this->parents();
  }

  public function get_parents(){
    $id = $this->request->data['id'];
    $parent_ids = explode(',', $id);
    $childrens = array();
    foreach($parent_ids as $parent_id){
      $childrens_array = $this->Group->children($parent_id);
      foreach($childrens_array as $array){
        $parent = $this->Group->find('first', array('conditions' => array('Group.id' => $parent_id)));
        $array['Group']['parent_name'] = $parent['Group']['name'];
        array_push($childrens, $array);
      }     
    }
    if(!empty($childrens)){
      $this->layout = 'ajax';
      $this->set('parent_id', $parent_ids);
      $this->set('lang', $this->request->data['lang']);
      $this->set('childrens', $childrens);

    }else{
        $id = $this->request->data['id'];
        $parent = $this->Group->find('first', array('conditions' => array('Group.id' => $id)));
        $array['Group']['parent_name'] = $parent['Group']['name'];
        $array['Group']['parent_id'] = $id;
        $array['Group']['id'] = $id;
        $array['Group']['name'] = $parent['Group']['name'];
        array_push($childrens, $array);
        
        $this->layout = 'ajax';
        $this->set('parent_id', array($id));
        $this->set('lang', $this->request->data['lang']);
        $this->set('childrens', $childrens);
    }
  }

  public function get_child(){
    $id = $this->request->data['id'];   
    $next = ($this->request->data['next'] + 1);   
    $childrens = $this->Group->children($id, true);
    $this->layout = 'ajax';
    $this->set('childrens', $childrens);
    $this->set('next', $next);
  }

  public function validate_audit(){
    $this->autoRender = FALSE;
    $action = $this->request->data['action'];
    $id     = $this->request->data['id'];
    $value  = $this->request->data['value'];

    if($action == '0'){
      $data = array(
        'id'      => $id,
        'status'  => $value
      );
      $this->Auditoria->save($data);
    }else{
      $data = array(
        'id'      => $id,
        'validate'  => $value
      );
      $this->Audit_answer->save($data);
    }
  }

  public function delete(){
    $this->autoRender  = FALSE;
    if ($this->request->is('POST')) {
      $table  = $this->request->data['table'];
      $id     = $this->request->data['id'];
      $this->loadModel('Question');
      if($table == 'User'){
        $this->User->delete($id);

      }else if($table == 'Help'){
        $this->loadModel('Help');
        $this->Help->delete($id);

      }else if($table == 'Auditoriav'){
        $this->Audit_answer->delete($id);

      }else if($table == 'Auditoriatp'){
        $this->Auditoria->delete($id);
        $this->Question->deleteAll(array('audit_id' => $id));

      }else if($table == 'Auditoria'){
        $this->Auditoria->delete($id);
        $this->Audit_answer->deleteAll(array('id_audit' => $id));
        $this->Question->deleteAll(array('audit_id' => $id));

      }else if($table == 'Audit_type'){
        $this->Audit_type->delete($id);

      }else if($table == 'dealers'){
        $this->loadModel('Dealer');
        $this->Dealer->delete($id);
      }
    }
  }

  public function export(){    
    $data = $this->request->data;
    foreach ($data['questions'] as $key => $question) {
      $respuestas[$key] = $this->Audit_answer->find('all', array(
        'conditions' => array(
          'Audit_answer.id' => $question)));
      foreach($respuestas[$key] as $respuesta){
        $this->loadModel('Question');
        $questions_array[$key] = $this->Question->find('all', array(
          'conditions' => array('Question.audit_id' => $respuesta['Auditoria']['id'])));
      }      
    }
    
    //error_reporting(E_ALL);

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set properties
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
    $objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
    $objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
    $objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
    $objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");

    $i=0;
    foreach ($respuestas as $key => $page) {
      if($i != 0){
        $objPHPExcel->createSheet($i);
      }
      if($page['0']['Auditoria']['sales'] == '1'){
        $this->loadModel('Sale');
        $sales = $this->Sale->find('all');
      }else{
        $sales = array();
      }
        $objPHPExcel->setActiveSheetIndex($i);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Audit Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Audit Type');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Date responded');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Worldzone');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Region ');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Distrito');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Dealer');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Dirección Dealer');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Teléfono Dealer');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Estado');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Fecha modificación');
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Auditor');
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Compañía Auditor');
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Teléfono auditor');
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Email auditor');
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Notes');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'ASO');
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'ASO Total');
        $letter = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'AAA', 'AAB', 'AAC', 'AAD', 'AAE', 'AAF', 'AAG', 'AAH', 'AAI', 'AAJ', 'AAK', 'AAL', 'AAM', 'AAN', 'AAO', 'AAP', 'AAQ', 'AAR', 'AAS', 'AAT', 'AAU', 'AAV', 'AAW', 'AAX', 'AAY', 'AAZ', 'AAB', 'ABB', 'ACB', 'ADB', 'AEB', 'AFB', 'AGB', 'AHB', 'AIB', 'AJB', 'AKB', 'ALB', 'AMB', 'ANB', 'AOB', 'APB', 'AQB', 'ARB', 'ASB', 'ATB', 'AUB', 'AVB', 'AWB', 'AXB', 'AYB', 'AZBS');
        $letter_i = '0';
        foreach ($questions_array[$key] as $question_info) {
          $objPHPExcel->getActiveSheet()->SetCellValue($letter[$letter_i].'1', $question_info['Question']['question']);
          $letter_i++;
        }
        foreach($sales as $sale){
          $objPHPExcel->getActiveSheet()->SetCellValue($letter[$letter_i].'1', $sale['Sale']['question']);
          $letter_i++;
        }

        $audit_i='2';
        foreach ($page as $audit) {
          $sales_questions = explode('/', $audit['Audit_answer']['sales']);
          $aso = '0';
          $total_sales = '0';
          foreach($sales_questions as $sales){
            if(!empty($sales)){
              $aso += '10';
              $sale_answer = explode(';', $sales);
              if($sale_answer['1'] == '1'){
                $total_sales += '10';
              }
            }else{
              $aso = '0';
              $total_sales = '0';
            }            
          }

          $questions = explode('/', $audit['Audit_answer']['answers']);
          $answer_response = array();
          foreach($questions as $answer){
            $answer_response[] = explode(';', $answer);            
          }

          $questions_aso = explode('/', $audit['Audit_answer']['sales']);
          $answers_aso = array();
          foreach($questions_aso as $question_aso){
            $answers_aso[] = explode(';', $question_aso);            
          }

          $parents = $this->Group->getPath($audit['Group']['id']);
          if(!array_key_exists('2', $parents)){
            $parents['2']['Group']['name'] = '';
            $parents['3']['Group']['name'] = '';
          }
          if ($audit['Audit_answer']['status'] == '0'){
            $status = 'Completed';
          }elseif($audit['Audit_answer']['status'] == '1'){
            $status = 'Validated';
          }elseif($audit['Audit_answer']['status'] == '3'){
            $status = 'To Validate';
          }else{
            $status = 'Rejected';
          }
          $audit_t = $this->Audit_type->find('first', array('conditions' => array('Audit_type.id' => $audit['Auditoria']['audit_type'])));
          $this->loadModel('Profile');
          $profile = $this->Profile->find('first', array('conditions', array('Profile.id' => $audit['User']['id'])));
          $objPHPExcel->getActiveSheet()->SetCellValue('A'.$audit_i, $audit['Auditoria']['title']);
          $objPHPExcel->getActiveSheet()->SetCellValue('B'.$audit_i, $audit_t['Audit_type']['name']);
          $objPHPExcel->getActiveSheet()->SetCellValue('C'.$audit_i, $audit['Audit_answer']['date']);
          $objPHPExcel->getActiveSheet()->SetCellValue('D'.$audit_i, array_key_exists('1', $parents) ? $parents['1']['Group']['name'] : '');
          $objPHPExcel->getActiveSheet()->SetCellValue('E'.$audit_i, $parents['2']['Group']['name']);
          $objPHPExcel->getActiveSheet()->SetCellValue('F'.$audit_i, $parents['3']['Group']['name']);
          $objPHPExcel->getActiveSheet()->SetCellValue('G'.$audit_i, $audit['Dealer']['name']);
          $objPHPExcel->getActiveSheet()->SetCellValue('H'.$audit_i, $audit['Dealer']['address']);
          $objPHPExcel->getActiveSheet()->SetCellValue('I'.$audit_i, $audit['Dealer']['phone']);
          $objPHPExcel->getActiveSheet()->SetCellValue('J'.$audit_i, $status);
          $objPHPExcel->getActiveSheet()->SetCellValue('K'.$audit_i, $audit['Audit_answer']['date']);
          $objPHPExcel->getActiveSheet()->SetCellValue('L'.$audit_i, $profile['Profile']['name']);
          $objPHPExcel->getActiveSheet()->SetCellValue('M'.$audit_i, $profile['Profile']['company']);
          $objPHPExcel->getActiveSheet()->SetCellValue('N'.$audit_i, $profile['Profile']['phone']);
          $objPHPExcel->getActiveSheet()->SetCellValue('O'.$audit_i, $profile['Profile']['email']);
          $objPHPExcel->getActiveSheet()->SetCellValue('P'.$audit_i, $audit['Audit_answer']['notes']);
          $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$audit_i, $aso);
          $objPHPExcel->getActiveSheet()->SetCellValue('R'.$audit_i, $total_sales);
          $sales_i = '0';
          pr($answer_response);die;
          foreach ($answer_response as $response) {
            if(!empty($response['0'])){
              $objPHPExcel->getActiveSheet()->SetCellValue($letter[$sales_i].$audit_i, $response['1']);
              $sales_i++;
            }
          }

          foreach ($answers_aso as $answer_aso) {
            if(!empty($answer_aso['0'])){
              $objPHPExcel->getActiveSheet()->SetCellValue($letter[$sales_i].$audit_i, $answer_aso['2']);
              $sales_i++;
            }
          }

          $audit_i++;
        }
        $objPHPExcel->getActiveSheet()->setTitle('Audit');
      
      $i++;
    }

    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $objWriter->save(str_replace(__FILE__, '/var/www/harley/app/webroot/files/audits_recent_updated.xlsx', __FILE__));
  }

  public function rmv_question(){
    $this->autoRender  = FALSE;
    $this->loadModel('Question');
    if($this->Question->delete($this->request->data('id'))){
      echo 'done';
    }else{
      echo 'not done';
    }
  }

  public function get_audits($id = Null){
    $parent_id = explode(',',$this->Auth->user('group_id'));
    $Profile = $this->Auth->user('Profile');
    $dealer = array($Profile['dealer'], '0');

    $audit_types = $Profile['audit_type'];
    if(empty($audit_types)){
      $audit_types_array = array();
    }else{
      $audit_types_array = explode(',', $audit_types);
    }

    foreach ($parent_id as $key => $parent) {
      $children = $this->Group->children($parent);


        $children[] = array(
            'Group' => array(
            'id' => 25,
            'name' => 'Global'
            )
            );

      if($parent != 25) {
        $children[] = array(
            'Group' => array(
            'id' => $parent
            )
            );
      }
      foreach ($children as $key => $value) {
        $childrens[] = $value;
      }      
    }

    if($id == 'modified'){
      if(!empty($childrens)){
        $audits_answerss = array();

        foreach ($childrens as $children) {
          if(!empty($audit_types_array)){
            $audit = $this->Audit_answer->find('all', array(
              'conditions' => array(
                'Audit_answer.group_id' => $children['Group']['id'],
                'Auditoria.audit_type' => $audit_types_array,
              )
            ));
          }else{
            $audit = $this->Audit_answer->find('all', array(
              'conditions' => array(
                'Audit_answer.group_id' => $children['Group']['id'],
              )
            ));

          }    
          foreach ($audit as $audi) {
            $audit_type = $this->Audit_type->find('first', array(
              'conditions' => array(
                  'id' => $audi['Auditoria']['audit_type'],
                )
              ));
            $audit_type_array = array('id' => $audit_type['Audit_type']['id'], 'name' => $audit_type['Audit_type']['name']);
            $audi['Audit_type'] = $audit_type_array;
            if (!empty($audi)) {
              $audits_answerss[] = $audi;
            }
          }
          
        }
      }else{
        $audits = $this->Audit_answer->find('all', array(
          'conditions' => array(
            'Audit_answer.group_id' => $parent_id, 
            'Audit_answer.id_dealer' => $dealer
            )
          ));
        $audits_answers = array();
        foreach ($audits as $audi) {
          $audit_type = $this->Audit_type->find('first', array(
            'conditions' => array(
                'id' => $audi['Auditoria']['audit_type'],
              )
            ));
          $audit_type_array = array('id' => $audit_type['Audit_type']['id'], 'name' => $audit_type['Audit_type']['name']);
          $audi['Audit_type'] = $audit_type_array;
          $audits_answerss[] = $audi;
        }
      }
      
      if(empty($audits_answerss)){
        $audits_answerss = array();
      }
      return $audits_answerss;

    }else if($id == 'published'){
      if(!empty($childrens)){

        $audits = array();
        foreach ($childrens as $children) {
            if($this->Auth->user('role') == '1') {
                $audit = $this->Auditoria->find('all', array(
                    'conditions' => array(
                    'Auditoria.dealer' => array($Profile['dealer'])
                )));
            } else {
                $audit = $this->Auditoria->find('all', array(
                'conditions' => array(
                    'Auditoria.group_id' => $children['Group']['id'],
                )));
            }

            if(!empty($audit)){
                $audits[] = $audit;
            }
        }
      }else{
        if($this->Auth->user('role') == '1') {
                $audits[] = $this->Auditoria->find('all', array(
                    'conditions' => array(
                    'Auditoria.dealer' => array($Profile['dealer'])
                )));
        } else {
            $audits[] = $this->Auditoria->find('all', array(
            'conditions' => array(
                'Auditoria.group_id' => $parent_id,
                'Auditoria.dealer'   => $dealer,
                )
            ));
        }
      }

      $x=0; $filtered_audits = array();

      foreach($audits as $audit) {
          $i=0;
          foreach($audit as $au) {
            $audit_type = $this->Audit_type->find('all', array(
                'conditions' => array(
                    'id' => explode(',',$au['Auditoria']['audit_type']),
                    )
                ));
            
            if(!count(array_diff(explode(',',$au['Auditoria']['audit_type']), $audit_types_array)) > 0) {
                $audits[$x][$i]['Audit_type'] = $audit_type;
                $i++;
            } else {
                unset($audits[$x]);
            }
          }
          $x++;
      }
      return $audits;

    } 
    
  }

  public function usercheck() {
      $this->autoRender  = FALSE;
      $user = $this->User->find('first', array('conditions' => array('User.username' => $this->request->data['username'])));
      if($user) {
          echo "1";
      } else {
          echo "0";
      }
  }

  public function get_audit_type(){
    $Profile = $this->Auth->user('Profile');
    $audit_types = $Profile['audit_type'];
    if(!empty($audit_types)){
      $audit_types_array = explode(',', $audit_types);

      $audit_type_filtered = $this->Audit_type->find('all', array(
        'conditions' => array(
          'id' => $audit_types_array
          )
        ));
    }else{
      $audit_type_filtered = $this->Audit_type->find('all');
    }


    return $audit_type_filtered;    
  }

  public function get_dealers(){
    $this->layout = 'ajax';
    $this->loadModel('Dealer');
    $dealers = $this->Dealer->find('all', array(
      'conditions' => array(
        'Dealer.group_id' => $this->request->data['id']
        )
      ));
    $this->set('dealers', $dealers);
  }

  public function aso($number = false){
    $this->autoRender  = FALSE;
    if($number <= 50){
      return 'urban';
    }else if($number >= 51 AND $number <= 100){
      return 'small';
    }else if($number >= 101 AND $number <= 150){
      return 'msmall';
    }else if($number >= 151 AND $number <= 200){
      return 'medium';
    }else if($number >= 201 AND $number <= 300){
      return 'mlarge';
    }else if($number >= 201 AND $number <= 400){
      return 'large';
    }else if($number >= 401){
      return 'vlarge';
    }
  }

  public function parents($id){
    $parent_id = $this->Groups_user->find('all', array('conditions' => array('Groups_user.user_id' =>  $this->Auth->user('id'))));
    foreach($parent_id as $llave_g){
      $id_group = $llave_g['Groups_user']['group_id'];
      $each_id[] = $id_group;
      $his_groups[] = $this->Group->find('all', array('conditions' => array('Group.id' => $id_group)));
    }

    var_dump($each_id);

    $childrens_g= $this->Group->getPath("314");
    $childrens_i= $this->Group->getPath($id);
    $last_key   = count($childrens_g) - 1;    
    $childrens_p = $this->Group->children("314");
    $this->set('p', $childrens_p);
    $this->set('g', $childrens_g);
    $this->set('i', $childrens_i);
    $this->set('his_groups', $his_groups);
    $this->set('last_key', $last_key);
    $this->set('each_id', $each_id);

    $times = array('1', '2', '3', '4');
    $this->set('times', $times);
  }

  /************************** Auditorias ************************/

  public function audits() {
    $dealers = $ids = array();
    $audits = $this->get_audits('published');
    $this->set('audits', $audits);

    $auditss = $this->get_audits('modified');

    foreach($auditss as $audit) {
        if(!in_array($audit['Dealer']['id'],$ids)) {
            $dealers[] = array('id' => $audit['Dealer']['id'], 'name' => $audit['Dealer']['name']);
        }
        $ids[] = $audit['Dealer']['id'];
    }

    $this->set('validate_audits', $auditss);
    $this->set('dealers', $dealers);

    $audit_types = $this->get_audit_type();
    $this->set('audit_types', $audit_types);
  }

  public function audit($id = null){
    $this->loadModel('Question');

    if($this->request->is('POST')){
      $areas = explode(',', $this->request->data['group_id']);
      foreach($areas as $area){
        if(array_key_exists('sales', $this->request->data)){
          $sales = $this->request->data['sales'];
        }else{
          $sales = '0';
        }
        if(array_key_exists('dealer', $this->request->data)){
          $dealer = $this->request->data['dealer'];
        }else{
          $dealer = '0';
        }

        if(array_key_exists('audit_type', $this->request->data)){
          $audit_type = implode(',', $this->request->data['audit_type']);

        }else{
          $audit_type = '0';
        }
        $data = array(
          'title' => $this->request->data['title'],
          'group_id' => $area,
          'status' => $this->request->data['status'],
          'auditor' => $this->request->data['auditor'],
          'audit_type' => $audit_type,
          'post_date' => $this->request->data['post_date'],
          'dealer' => $dealer,
          'sales' => $sales,
        );
        if (!array_key_exists('id', $this->request->data)) {
          $this->Auditoria->create();
        }else{
          $data['id'] = $this->request->data['id'];
        }
        if($this->Auditoria->save($data)){
          foreach($this->request->data['questions'] as $key => $questions){
            foreach ($questions as $question) {

              if (!array_key_exists('response_required', $question)) {
                $question['response_required'] = '0';
              }

              if(!array_key_exists('question',$question)) {
                  $this->Question->delete($question['id']);
              } else {
              
                if($question['open_response'] == '0'){
                    $open = '0';
                    $yes_no = '1';
                }else{
                    $open = '1';
                    $yes_no = '0';
                }

                $question_data = array(
                    'id'        => $question['id'],
                    'audit_id'  => $this->Auditoria->id,
                    'question'  => $question['question'],
                    'required'  => $question['response_required'],
                    'open'      => $open,
                    'yes_no'    => $yes_no,
                    'group_name'=> $key
                );

                if ($id == '') {
                $this->Question->create();
                }
                $this->Question->save($question_data);
              }
            }
          }
        }        
      }
      if(array_key_exists('id', $data)) { $action = 'update'; } else { $action = 'create'; }
      $this->redirect(array('controller' => 'admin', 'action' => 'audits/'.$action.'/success/'.$this->request->data['title']));
    }
    if(empty($id)){
      $auditoria = null;
      $this->parents($id);
    }else{
      $auditoria = $this->Auditoria->find('first', array(
          'conditions' => array(
              'Auditoria.id' => $id
            )
        ));
      $this->parents($auditoria['Auditoria']['group_id']);
      $questions_array = $this->Question->find('all', array(
          'conditions' => array(
              'Question.audit_id' => $auditoria['Auditoria']['id']
            )
        ));
      $questions = array();
      foreach ($questions_array as $question_array) {
        $group_name = $question_array['Question']['group_name'];
        $questions[$group_name][] = $question_array;
      }
      $auditoria['Questions'] = $questions;     
    }
    $children = $this->Group->children('25');
    $this->set('au', $auditoria);

    $this->set('childrens', $children);
    $this->set('parent_id', '25');
    $this->set('user_id', $this->Auth->user());

    $audit_types = $this->get_audit_type();
    $this->set('audit_types', $audit_types);
  }

  /************************** Areas *****************************/
  public function areas(){
    $parent_id = explode(',',$this->Auth->user('group_id'));
    $childrens = array();
    foreach ($parent_id as $key => $parent) {
      $children = $this->Group->children($parent);
      foreach ($children as $key => $value) {
        $childrens[] = $value;
      }      
    }
    $this->set('areas', $childrens);
  }

  public function area($id){
    if($this->request->is('POST')){
      $this->Group->id = $this->request->data['id']; // id of Extreme knitting
      $this->Group->save(array('name' => $this->request->data['name']));
      $this->redirect(array('controller' => 'admin', 'action' => 'areas'));
    }
    $area = $this->Group->find('first', array('conditions' => array('Group.id' => $id)));
    $this->set('area', $area);
  }

  /************************** Auditores *************************/

  public function auditores(){
    $profile = $this->Auth->user();
    $role = $profile['role'];
    $role_user = $this->User->find('all', array('conditions' => array('User.role <' => $role)));
    $this->set('users', $role_user);
  }

  public function auditor($id = null){
    if($this->request->is('POST')){
      $this->autoRender  = FALSE;
      $error = '';
      if(empty($this->request->data['username'])){
        $error .= '<b>Username</b> Cannot be empty<br>';
      }
      if(empty($this->request->data['name'])){
        $error .= '<b>Name</b> Cannot be empty<br>';
      }
      if(empty($this->request->data['email'])){
        $error .= '<b>Email</b> Cannot be empty<br>';
      }
      if(empty($this->request->data['id'])){
        if(empty($this->request->data['password'])){
          $error .= '<b>Password</b> Must be generated<br>';
        }
      }
      if(empty($this->request->data['last_selected'])){
        $error .= '<b>Area</b> Must be selected<br>';
      }
      if(empty($this->request->data['role'])){
        $error .= '<b>role</b> Must be selected<br>';
      }
      if($this->request->data['role'] == '1'){
        if(empty($this->request->data['dealer'])){
          $error .= '<b>Dealer</b> Must be selected<br>';
        }        
      }
      if(!empty($error)){
        echo "<div class='nNote nFailure hideit'>
                <p><strong>ERROR: </strong><br>".$error."</p>
              </div>";
      }else{
        if(empty($this->request->data['id'])){
          $data = array(
            'User' => array(
              'username'  => $this->request->data['username'],
              'password'  => $this->request->data['password'],
              'role'      => $this->request->data['role'],
              'group_id'  => $this->request->data['last_selected'],
              'status'    => $this->request->data['status']
              ),
            'Profile' => array(
              'name'      => $this->request->data['name'],
              'audit_type'=> $this->request->data['audit'],
              'email'     => $this->request->data['email'],
              'lang'      => $this->request->data['lang'],
              'company'   => $this->request->data['company'],
              'note'      => $this->request->data['note'],
              'dealer'    => $this->request->data['dealer'],
              )
            );
          $this->User->create();
        }else{
          if (!empty($this->request->data['password'])) {
            $data = array(
            'User' => array(
              'id'        => $this->request->data['id'],
              'username'  => $this->request->data['username'],
              'password'  => $this->request->data['password'],
              'role'      => $this->request->data['role'],
              'group_id'  => $this->request->data['last_selected'],
              'status'    => $this->request->data['status']
              ),
            'Profile' => array(
              'id'        => $this->request->data['id_p'],
              'name'      => $this->request->data['name'],
              'audit_type'=> $this->request->data['audit'],
              'email'     => $this->request->data['email'],
              'lang'      => $this->request->data['lang'],
              'company'   => $this->request->data['company'],
              'note'      => $this->request->data['note'],
              'dealer'    => $this->request->data['dealer'],
              )
            );
          }else{
            $data = array(
            'User' => array(
              'id'        => $this->request->data['id'],
              'username'  => $this->request->data['username'],
              'role'      => $this->request->data['role'],
              'group_id'  => $this->request->data['last_selected'],
              'status'    => $this->request->data['status']
              ),
            'Profile' => array(
              'id'        => $this->request->data['id_p'],
              'name'      => $this->request->data['name'],
              'audit_type'=> $this->request->data['audit'],
              'email'     => $this->request->data['email'],
              'lang'      => $this->request->data['lang'],
              'company'   => $this->request->data['company'],
              'note'      => $this->request->data['note'],
              'dealer'    => $this->request->data['dealer'],
              )
            );
          }
        }   
        if ($this->User->saveAssociated($data)){
          if(!empty($this->request->data['password'])){
            $email    = new CakeEmail();
            $email->viewVars(array('username' => $this->request->data['username'], 'password' => $this->request->data['password'] ))
                ->template('userpass')
                ->emailFormat('html')
                ->config(array('from' => array('contact@harley.com' => 'Road to excellence') ,'to' => $this->request->data['email']))
                ->subject ('User Password')
                ->send();
            echo "1";
          }else{
            echo "2";
          }
          
        }
      }        
    }
    $audit_types = $this->Audit_type->find('all');
    $this->set('audit_types', $audit_types);
    if($id){
      $auditor = $this->User->find('first', array(
        'conditions' => array(
          'User.id' => $id,
          )
        ));
      $this->parents($auditor['User']['group_id']);
    }else{
      $auditor = null;
      $this->parents($id);
    }
    $this->set('id', $auditor);
  }

  public function auditor_import(){
    $errors = array();
    if($this->request->is('POST')){
      if($this->request->params['form']['file']['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
        if(move_uploaded_file($this->request->params['form']['file']['tmp_name'], WWW_ROOT.'files/imports/'.$this->request->params['form']['file']['name'])){
          
          $inputFileName = WWW_ROOT.'files/imports/'.$this->request->params['form']['file']['name'];
          $xlsx = new SimpleXLSX($inputFileName);          
          foreach($xlsx->rows() as $key => $sale){
            if(empty($sale)){
              unset($sale[$key]);
            }
            if($key == '0'){
              if ($sale['0'] != 'username') {
                $errors['Columns']['Headers'][] = 'This excel does not contain username column';
              }
            }else if(empty($errors)){
              $audit_type = $this->Audit_type->find('first', array(
                'conditions' => array(
                  'name' => $sale['2']
                  )
                ));
              if (empty($audit_type)) {
                $errors['Rows'][$sale['0']][] = 'The audit type you enter is not in the database.';
              }
              if($sale['7'] == '2'){
                if(empty($sale['10'])){
                  $errors['Rows'][$sale['0']][] = 'The User needs to have a district.';
                }
              }
              if(!empty($sale['10'])){
                $wolrdzone = $this->Group->find('first', array(
                'conditions' => array(
                  'name' => $sale['10']
                  )
                ));
              }else if(!empty($sale['9'])){
                $wolrdzone = $this->Group->find('first', array(
                'conditions' => array(
                  'name' => $sale['9']
                  )
                ));
              }else if(!empty($sale['8'])){
                $wolrdzone = $this->Group->find('first', array(
                'conditions' => array(
                  'name' => $sale['8']
                  )
                ));
              }else{
                $errors['Rows'][$sale['0']][] = 'The User has no Area.';
              }
              if(empty($errors)){
                if ($sale['7'] == '1') {
                  $status = '0';
                }else{
                  $status = '1';
                }
                $pass = $this->gen_pass();
                $data = array(
                  'User' => array(
                      'username' => $sale['0'],
                      'password' => $pass,
                      'role' => $sale['7'],
                      'created' => date('Y-m-d G:i:s'),
                      'modified' => date('Y-m-d G:i:s'),
                      'group_id' => $wolrdzone['Group']['id'],
                      'status' => $status,
                  ),
                  'Profile' => array(
                      'name' => $sale['1'],
                      'audit_type' => $audit_type['Audit_type']['id'],
                      'dealer' => null,
                      'email' => $sale['3'],
                      'lang' => $sale['4'],
                      'company' => $sale['5'],
                      'note' => $sale['6'],
                      'phone' => $sale['11'],
                  )                
                );              
                $this->loadModel('User');
                $this->User->create();
                if($this->User->saveAssociated($data)){
                  $Email = new CakeEmail();
                  $Email->from(array('contact@harley.com' => 'Road to excellence'));
                  $Email->to($sale['3']);
                  $Email->subject('User Password');
                  $Email->send('Your username is "'.$sale['0'].'" and your password is "'.$pass.'"');
                }
              }
            }
          }
        }
      }
    }
    $this->set('errors', $errors);
  }

  /************************** Dealers ***************************/

  public function dealers(){
    $profile = $this->Auth->user();
    $group_id = $profile['group_id'];
    $group_ids = explode(',', $group_id);
    $dealers = array();
    foreach ($group_ids as $g_id) {
      $childrens = $this->Group->children($g_id);
      foreach ($childrens as $children) {
        $this->loadModel('Dealer');
        $user = $this->Dealer->find('all', array('conditions' => array('Dealer.group_id' => $children['Group']['id'])));
        if(!empty($user)){
          $dealers[] = $user;
        }
      }
    }
    $this->set('dealers', $dealers);
  }

  public function dealer($id = null){
    $this->loadModel('Dealer');
    if($this->request->is('POST')){      
      $this->autoRender  = FALSE;
      $data = array(
        'name'      => $this->request->data['name'],
        'manager'  => $this->request->data['manager'],
        'phone'  => $this->request->data['phone'],
        'address'   => $this->request->data['address'],
        'group_id'  => $this->request->data['last_selected'],        
        'size'  => $this->request->data['size']
      );
      if(!empty($this->request->data['id'])){
        $data['id'] = $this->request->data['id'];
      }
      $this->Dealer->create();
      if ($this->Dealer->save($data)) {
          Echo '<div class="nNote nSuccess hideit">
                  <p><strong>Verificado: </strong>It has been saved!!!!</p>
                </div>';
      }   
    }
    $parent_id = $this->Auth->user('group_id');
    $childrens = $this->Group->children($parent_id);
    $this->set('childrens', $childrens);
    $this->set('parent_id', $parent_id);
    
    if($id){
      $auditor = $this->Dealer->find('first', array(
        'conditions' => array(
          'Dealer.id' => $id,
          )
        ));
      $this->parents($auditor['Dealer']['group_id']);
    }else{
      $auditor = null;
      $this->parents($id);
    }
    $this->set('id', $auditor);
  }

  public function dealer_import(){
    $errors = array();
    if($this->request->is('POST')){
      if($this->request->params['form']['file']['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
        if(move_uploaded_file($this->request->params['form']['file']['tmp_name'], WWW_ROOT.'files/imports/'.$this->request->params['form']['file']['name'])){
          
          $inputFileName = WWW_ROOT.'files/imports/'.$this->request->params['form']['file']['name'];
          $xlsx = new SimpleXLSX($inputFileName);
          $ok = array();         
          foreach($xlsx->rows() as $key => $sale){
            if(empty($sale)){
              unset($sale[$key]);
            }
            if($key == '0'){
              if ($sale['0'] != 'name') {
                $errors['Columns']['Headers'][] = 'This excel does not contain "name" column';
              }
              if ($sale['1'] != 'manager') {
                $errors['Columns']['Headers'][] = 'This excel does not contain "manager" column';
              }
              if ($sale['2'] != 'phone') {
                $errors['Columns']['Headers'][] = 'This excel does not contain "phone" column';
              }
              if ($sale['3'] != 'address') {
                $errors['Columns']['Headers'][] = 'This excel does not contain "address" column';
              }
              if ($sale['4'] != 'aso') {
                $errors['Columns']['Headers'][] = 'This excel does not contain "aso" column';
              }
            }else if(empty($errors)){
              if(!empty($sale['7'])){
                $wolrdzone = $this->Group->find('first', array(
                'conditions' => array(
                  'name' => $sale['7']
                  )
                ));
              }else if(!empty($sale['6'])){
                $wolrdzone = $this->Group->find('first', array(
                'conditions' => array(
                  'name' => $sale['6']
                  )
                ));
              }else if(!empty($sale['5'])){
                $wolrdzone = $this->Group->find('first', array(
                'conditions' => array(
                  'name' => $sale['5']
                  )
                ));
              }else{
                $errors['Rows'][$sale['0']][] = 'The User has no Area.';
              }              
              if(empty($errors)){              
                $data = array(
                  'name' => $sale['0'],
                  'manager' => $this->gen_pass(),
                  'phone' => $sale['7'],
                  'address' => date('Y-m-d G:i:s'),
                  'size' => date('Y-m-d G:i:s'),
                  'group_id' => $wolrdzone['Group']['id'],
                );              
                $this->loadModel('Dealer');
                $this->Dealer->create();
                if($this->Dealer->save($data)){
                }else{
                  $ok[] = 'No';
                }
              }
            }
          }
          if(empty($ok)){
            $this->redirect(array('controller' => 'admin', 'action' => 'dealers'));
          }
        }
      }
    }
    $this->set('errors', $errors);
  }

  /************************** detail ****************************/

  public function detail($id = null){
    if(!empty($id)){
      $this->loadModel('Question');
      $this->loadModel('Sale');
      $audit = $this->Audit_answer->find('first', array('conditions' => array('Audit_answer.id' => $id)));
      $answers = explode('/', $audit['Audit_answer']['answers']);      
      $questions = array();
      foreach ($answers as $key => $answer) {
        if (!empty($answer)) {
          $answer_array = explode(';', $answer);
          $question = $this->Question->find('first', array('conditions' => array('id' => $answer_array['0'])));
          $question['Answer'] = $answer_array;
          $group_name = $question['Question']['group_name'];
          $questions[$group_name][$key] = $question;
        }
      }
      $sales_answers = explode('/', $audit['Audit_answer']['sales']);
      foreach ($sales_answers as $key => $sale_q) {
        if(!empty($sale_q)){
          $sales_array = explode(';', $sale_q);
          $sale_question = $this->Sale->find('first', array('conditions' => array('Sale.id' => $sales_array['0'])));
          $sale_question['Answer'] = $sales_array;
          $sale['Sales Target'][$key] = $sale_question;
        }else{
          $sale = array();
        }
      }
      $audit['Answers'] = $questions;
      $audit['Sales'] = $sale;
      $this->set('audit', $audit);
    }
  }

  /************************** Help ******************************/
  public function helps(){
    $this->loadModel('Help');
    $helps = $this->Help->find('all');
    $this->set('helps', $helps);
  }

  public function help($id = null){
    $this->loadModel('Help');
    if($this->request->is('POST')){
      $this->Help->create();
      $this->Help->save($this->request->data);
      $this->redirect(array('controller' => 'admin', 'action' => 'helps'));
    }
    if(!empty($id)){
      $help = $this->Help->find('first', array(
        'conditions' => array(
          'id' => $id
        )
      ));        
    }else{
      $help = null;
    }
    $this->set('help', $help);
  }

  /************************** sales *****************************/
  public function sales_targets(){
    $this->loadModel('Sale');
    $sales = $this->Sale->find('all');
    $this->set('sales', $sales);
  }

  public function import_sales(){
    $errors = array();
    if($this->request->is('POST')){
      if($this->request->params['form']['file']['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
        if(move_uploaded_file($this->request->params['form']['file']['tmp_name'], WWW_ROOT.'files/imports/'.$this->request->params['form']['file']['name'])){
          
          $inputFileName = WWW_ROOT.'files/imports/'.$this->request->params['form']['file']['name'];
          $xlsx = new SimpleXLSX($inputFileName);          
          foreach($xlsx->rows() as $key => $sale){
            if($key == '0'){
              if ($sale['2'] != 'urban') {
                $errors['Columns']['Headers'][] = 'This Excel does not contain a "urban" column, and cannot continue.';
              }
              if ($sale['3'] != 'small') {
                $errors['Columns']['Headers'][] = 'This Excel does not contain a "small" column, and cannot continue.';
              }
              if ($sale['4'] != 'msmall') {
                $errors['Columns']['Headers'][] = 'This Excel does not contain a "msmall" column, and cannot continue.';
              }
              if ($sale['5'] != 'medium') {
                $errors['Columns']['Headers'][] = 'This Excel does not contain a "medium" column, and cannot continue.';
              }
              if ($sale['6'] != 'mlarge') {
                $errors['Columns']['Headers'][] = 'This Excel does not contain a "mlarge" column, and cannot continue.';
              }
              if ($sale['7'] != 'large') {
                $errors['Columns']['Headers'][] = 'This Excel does not contain a "large" column, and cannot continue.';
              }
              if ($sale['8'] != 'vlarge') {
                $errors['Columns']['Headers'][] = 'This Excel does not contain a "vlarge" column, and cannot continue.';
              }
            }else if(empty($errors)){
              $audit_type = $this->Audit_type->find('first', array(
                'conditions' => array(
                  'name' => $sale['1']
                  )
                ));
              if (empty($audit_type)) {
                $errors['Rows'][$sale['0']][] = 'The audit type you enter is not in the database.';
              }
              if(!is_numeric($sale['2'])){
                $errors['Rows'][$sale['0']][] = 'The column urban is not a number.';
              }
              if(!is_numeric($sale['3'])){
                $errors['Rows'][$sale['0']][] = 'The column small is not a number.';
              }
              if(!is_numeric($sale['4'])){
                $errors['Rows'][$sale['0']][] = 'The column msmall is not a number.';
              }
              if(!is_numeric($sale['5'])){
                $errors['Rows'][$sale['0']][] = 'The column medium is not a number.';
              }
              if(!is_numeric($sale['6'])){
                $errors['Rows'][$sale['0']][] = 'The column mlarge is not a number.';
              }
              if(!is_numeric($sale['7'])){
                $errors['Rows'][$sale['0']][] = 'The column large is not a number.';
              }
              if(!is_numeric($sale['8'])){
                $errors['Rows'][$sale['0']][] = 'The column vlarge is not a number.';
              }
              if(empty($errors)){
                $data = array(
                  'question' => $sale['0'],
                  'audit_type' => $audit_type['Audit_type']['id'],
                  'urban' => $sale['2'],
                  'small' => $sale['3'],
                  'msmall' => $sale['4'],
                  'medium' => $sale['5'],
                  'mlarge' => $sale['6'],
                  'large' => $sale['7'],
                  'vlarge' => $sale['8'],
                  );              
                $this->loadModel('Sale');
                $this->Sale->create();
                if ($this->Sale->save($data)) {
                  $this->redirect(array('controller' => 'admin', 'action' => 'sales_targets'));
                }
              }
            }
          }
        }
      }
    }
    $this->set('errors', $errors);
  }

  public function sales_target($id = null){
    $this->loadModel('Sale');
    if($this->request->is('POST')){
      $this->Sale->save($this->request->data);
      $this->redirect(array('controller' => 'admin', 'action' => 'sales_targets'));
    }
    if ($id) {
      $sales = $this->Sale->find('first', array(
        'conditions' => array(
          'Sale.id' => $id 
          )
        ));
      $sale = $sales;
    }else{
      $sale = null;
    }
    $this->set('sale', $sale);

    $audit_types = $this->Audit_type->find('all');
    $this->set('audit_types', $audit_types);
  }

  /************************** export ****************************/

  public function audit_types(){
    $audit_types = $this->Audit_type->find('all');
    $this->set('audit_types', $audit_types);
  }

  public function audit_type($id = null){
    if($this->request->is('POST')){
      $this->Audit_type->create();
      $this->Audit_type->save($this->request->data);
      if($id) { $action = 'update';} else { $action = 'create'; }
      $this->redirect(array('controller' => 'admin', 'action' => 'audit_types/'.$action.'/success/'.$this->request->data['name']));
    }
    if(!empty($id)){
      $audit_type = $this->Audit_type->find('first', array(
        'conditions' => array(
          'id' => $id
        )
      ));      
    }else{
      $audit_type = null;
    }   
    $this->set('audit_type', $audit_type);
  }

  public function import_areas(){
    if($this->request->is('POST')){
      if(move_uploaded_file($this->request->params['form']['file']['tmp_name'], WWW_ROOT.'files/imports/'.$this->request->params['form']['file']['name'])){
        
        $inputFileName = WWW_ROOT.'files/imports/'.$this->request->params['form']['file']['name'];
        $xlsx = new SimpleXLSX($inputFileName);
        $error = array();  
        foreach($xlsx->rows() as $key => $world){
          if($key != '0'){
            $worlds[$world['0']][] = $world;
          }
        }
        foreach ($worlds as $world) {
          foreach($world as $region){
            $region['name'] = $region['3'].'-'.$region['2'];
            $regions[$region['0']][$region['1']][] = $region;
          }          
        }
        foreach($regions as $wname => $worldzone){
          $dataw = array(
            'Group' => array(
              'parent_id' => '25',
              'name' => $wname
              )
            );
          $this->Group->create();
          $this->Group->save($dataw);
          $worldzone['id'] = $this->Group->id;
          foreach($worldzone as $rkey => $region){
            if($rkey != 'id'){
              $datar = array(
              'Group' => array(
                'parent_id' => $worldzone['id'],
                'name' => $rkey
                )
              );
              $this->Group->create();
              $this->Group->save($datar);
              $region['id'] = $this->Group->id;
              foreach ($region as $dkey => $district) {
                if($dkey != 'id'){
                  $datad = array(
                  'Group' => array(
                    'parent_id' => $region['id'],
                    'name' => $district['name']
                    )
                  );
                  $this->Group->create();
                  $this->Group->save($datad);
                }
              }
            }
          }
        }
      }
    }
  }

  public function reorder() {
    $this->autoRender  = FALSE;
    $this->Group->recover();
  }

  public function get_dealers_info(){
    $this->autoRender  = FALSE;
    $id = $this->request->data['id'];
    $this->loadModel('Dealer');
    $dealers = $this->Dealer->find('all', array(
      'conditions' => array('Dealer.group_id' => $id)));
    $select = '<select name="dealer" class="dealer_id">';
    $select .= '<option value="">Please select a Dealer</option>';
    foreach ($dealers as $dealer) {
      $select .= '<option value="'.$dealer['Dealer']['id'].'">'.$dealer['Dealer']['name'].'</option>';
    }
    $select .= '</select>';
    return $select;
  }

  public function send_mail(){
    $this->autoRender  = FALSE;
    $Email = new CakeEmail();
    $Email->from(array('contact@harley.com' => 'Road to excellence'));
    $Email->to('servidorv@gmail.com');
    $Email->subject('User Password');
    $Email->send('Your username is ');
  }

  public function test(){
    $this->autoRender  = FALSE;
    $audits = $this->Auditoria->find('all', array(
      'conditions' => array(
        'Auditoria.group_id LIKE' => '%145%'
        )
      )
    );
    pr($audits);
  }
}