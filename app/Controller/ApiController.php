<?php 
App::import('Controller', 'Admin');
class ApiController extends AppController {

  var $components = array("RequestHandler");

  public function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allow('login', 'index');
      $this->Auth->loginAction = array('controller' => 'api', 'action' => 'login');
      $this->Auth->loginRedirect = array('controller' => 'api', 'action' => 'index');
      $this->Auth->logoutRedirect = array('controller' => 'api', 'action' => 'login');
      $this->autoRender = false;
      $this->loadModel('User');
      $this->loadModel('Group');
      $this->loadModel('Auditoria');
      $this->loadModel('Audit_type');
      $this->loadModel('Dealer');
      $this->loadModel('Audit_answer');
      
  }

  public function index() {
    $this->RequestHandler->respondAs('application/json');
    $user_f_info = $this->Auth->user('User');
    $user_info = $this->User->find('first', array('conditions' => array('username' => $user_f_info['username'])));
    if(empty($user_info)){
      return '{"error" : 0}';
    }else{
      return '{"error" : 1}';
    }
  }
  
  public function login() {
    $this->RequestHandler->respondAs('application/json');
    if($this->request->is('post')){
      $login_info['User']['username'] = $this->request->data['username'];
      $login_info['User']['password'] = AuthComponent::password($this->request->data['password']);

      $this->Auth->login($login_info);
      $user_f_info = $this->Auth->user('User');
      $user_info = $this->User->find('first', array('conditions' => array('username' => $user_f_info['username'], 'password' => $login_info['User']['password'])));
    }else{
      $user_info = '0';
    }

    if(empty($user_info)){
      $error_response = array('error' => '1');
      return json_encode($error_response);
    }else{
      return json_encode($user_info);
    }
  }

  public function logout(){
    $this->RequestHandler->respondAs('application/json');
    if($this->Auth->logout()){
      $error_response = array('Success' => '1');
      return json_encode($error_response);
    }else{
      return json_encode($user_info);
    }
  }

  public function audits(){
    $this->RequestHandler->respondAs('application/json');
    $user_f_info = $this->Auth->user('User');
    $user_info = $this->User->find('first', array('conditions' => array('username' => $user_f_info['username'])));
    $dealer_id = $user_info['Profile']['dealer'];
    $group_id = explode(',', $user_info['User']['group_id']);
    $role = $user_info['User']['role'];
    $audit_types = $user_info['Profile']['audit_type'];
    $results = array();

    $audit_types = '1';

    if(empty($audit_types)){
      $audit_types_array = array();
    }else{
      $audit_types_array = explode(',', $audit_types);
    }

    $audit_answer_ids = array();
    foreach ($group_id as $key => $parent) {
      $children = $this->Group->children($parent);
      $children[] = array(
        'Group' => array(
          'id' => 25,
          'name' => 'Global'
          )
        );
      foreach ($children as $key => $value) {
        $group_tree[] = $value;
        $Audit_answer = $this->Audit_answer->find('all', array(
          'conditions' => array(
            'Dealer.group_id' => $value['Group']['id'],
            'Audit_answer.validate !=' => '2',
            )
          ));
        foreach ($Audit_answer as $value) {
          $audit_answer_ids[] = $value['Audit_answer']['id_audit'];
        }
      }
      if(!empty($children)){
        $group_tree[]['Group']['id'] = $parent;
      }    
    }

    $Audit_answerss = $this->Audit_answer->find('all', array(
      'conditions' => array(
        'Dealer.group_id' => $group_id,
        'Audit_answer.validate !=' => '2',
        )
      ));
    foreach ($Audit_answerss as $value) {
      $audit_answer_ids[] = $value['Audit_answer']['id_audit'];
    }

    if (!empty($group_tree)) {
      $childrens = $group_tree;
      foreach ($childrens as $children) {

        if($role == '1') {
            $dealers = $this->Dealer->find('all', array(
            'conditions' => array(
                'Dealer.id' => $dealer_id,
                )
            ));
        } else {
            $dealers = $this->Dealer->find('all', array(
            'conditions' => array(
                'Dealer.group_id' => $children['Group']['id'],
                )
            ));
        }

        if(count($dealers) == 0) {
            $dealers = $this->Dealer->find('all', array(
            'conditions' => array(
                'Dealer.group_id' => $children['Group']['id'],
                )
            ));
        }
        

        foreach ($dealers as $key => $dealer) {

            $group_ids = array($children['Group']['id']);

            if(!($role == '1' || $role == '2')) {
                $group_ids[] = '25';
            }

            if(array_key_exists('parent_id',$children['Group'])) {
                array_push($group_ids, $children['Group']['parent_id']);
            }
            

            
            if($role == '1') {
                $conditions = array("OR" => array('Auditoria.dealer' => array($dealer['Dealer']['id'])));
            } elseif($role == '2') {
                $conditions = array(
                    "AND" => array(
                    'Auditoria.group_id' => $group_ids,
                    'Auditoria.status'   => '1'),
                    array("OR" => array($dealer['Dealer']['id']))
                );            
            } else {
                $conditions = array(
                    "AND" => array(
                    'Auditoria.group_id' => $group_ids,
                    'Auditoria.status'   => '1',
                    "OR" => array('Auditoria.dealer' => array('0'))
                ));
            }

            $audits = $this->Auditoria->find('all', array(
                'conditions' => $conditions
            ));

            $filtered_audits = array();
            $i=0;
            foreach ($audits as $audit) {
                if (!in_array($audit['Auditoria']['id'], $audit_answer_ids)) {
                    if(count(array_diff(explode(',',$audit['Auditoria']['audit_type']), $audit_types_array)) >= 0) {
                        $audit['Auditoria']['audit_type'] = $audit['Auditoria']['audit_type'];
                        $filtered_audits[$i] = $audit;
                        $i++;
                    }
                }
            }

            $dealer['auditorias'] = $filtered_audits;
            $results[] = $dealer;
        }
      }
    }else{
      if(!empty($dealer_id)){
      $dealers = $this->Dealer->find('all', array(
        'conditions' => array(
          'Dealer.group_id' => $group_id,
          'Dealer.id' => $dealer_id
          )
        ));
      }else{
        $dealers = $this->Dealer->find('all', array(
        'conditions' => array(
          'Dealer.group_id' => $group_id,          
          )
        ));
      }    

      foreach ($dealers as $key => $dealer) {
        if(!empty($dealer_id)){
          $audits = $this->Auditoria->find('all', array(
            'conditions' => array(
              'Auditoria.group_id' => $group_id,
              'Auditoria.dealer'   => array($dealer_id, '0'),
              'Auditoria.status'   => '1'            
            )
          ));
            
        }else{
          $audits = $this->Auditoria->find('all', array(
            'conditions' => array(
              'Auditoria.group_id' => $group_id,
              'Auditoria.dealer'   => array($dealer['Dealer']['id'], '0'),
              'Auditoria.status'   => '1'            
            )
          ));
        }

        $filtered_audits = array();
        $i=0;
        foreach ($audits as $audit) {
            //var_dump($audit['Auditoria']['id'], $audit_answer_ids);
            if (!in_array($audit['Auditoria']['id'], $audit_answer_ids)) {

               // var_dump(count(array_diff(explode(',',$audit['Auditoria']['audit_type']), $audit_types_array)));

                if(count(array_diff(explode(',',$audit['Auditoria']['audit_type']), $audit_types_array)) >= 0) {
                    $audit['Auditoria']['audit_type'] = $audit['Auditoria']['audit_type'];
                    $filtered_audits[$i] = $audit;
                    $i++;
                }
            }
          }

        $dealer['auditorias'] = $filtered_audits;
        $results[$key] = $dealer;
      }
    };

       //     echo '<pre>';
       // var_dump($audits);
       // echo '</pre>';
    
    return '{ "response":'.json_encode($results).'}';
  }

  public function audit(){
    $this->RequestHandler->respondAs('application/json');
    if ($this->request->is('post')) {
      $auditoria = $this->Auditoria->find('first', array('conditions' => array('Auditoria.id' => $this->request->data['id'])));
      $this->loadModel('Question');
      $questions = array();
      $questions_f = $this->Question->find('all', array(
          'conditions' => array('audit_id' => $auditoria['Auditoria']['id'])
        ));
      foreach ($questions_f as $question) {
        $group_name = $question['Question']['group_name'];
        $questions[$group_name][] = $question;
      }
      $auditoria['Questions'] = $questions;

      if($auditoria['Auditoria']['sales'] == '1'){
        $this->loadModel('Sale');
        $sales = $this->Sale->find('all');
        foreach($sales as $key => $sale){
          $sale['Sale']['open'] = '1';
          $sale['Sale']['required'] = '1';
          $auditoria['Questions']['Sales Target'][]['Question'] = $sale['Sale'];
        }        
      }
      
      return '{"response": '.json_encode($auditoria).'}';
    }
  }

  public function help(){
    $this->RequestHandler->respondAs('application/json');
    $this->loadModel('Help');
    $helps = $this->Help->find('all');
    return '{"response": '.json_encode($helps).'}';
  }
  public function aso($number = false){
    $this->autoRender  = FALSE;
    if($number <= 50){
      return 'urban';
    }else if($number >= 51 AND $number <= 100){
      return 'small';
    }else if($number >= 101 AND $number <= 150){
      return 'msmall';
    }else if($number >= 151 AND $number <= 200){
      return 'medium';
    }else if($number >= 201 AND $number <= 300){
      return 'mlarge';
    }else if($number >= 201 AND $number <= 400){
      return 'large';
    }else if($number >= 401){
      return 'vlarge';
    }
  }

  public function add_audit(){
    $this->RequestHandler->respondAs('application/json');

    if($this->request->is('POST')){
      $this->loadModel('Audit_answer');
      $this->loadModel('Dealer');
      $this->loadModel('Sale');

      $dealer = $this->Dealer->find('first', array(
        'conditions' => array(
          'Dealer.id' => $this->request->data['id_dealer'])));
      $size = $this->aso($dealer['Group']['size']);
      
      if(!empty($this->request->data['sales'])){
        
        $sales_questions = explode('/', $this->request->data['sales']);
        foreach($sales_questions as $answer){
          if(!empty($answer)){
            $answer_separated = explode(';', $answer);
            $question = $this->Sale->find('first', array(
              'conditions' => array(
                'Sale.id' => $answer_separated['0'])));
            $res = $answer_separated['1'];
            if($res == $question['Sale'][$size]){
              $answer_array_verify = array($answer_separated['0'], '1', $answer_separated['1']);          
            }else{
              $answer_array_verify = array($answer_separated['0'], '0', $answer_separated['1']);
            }
            $answer_imploted[] = implode(';', $answer_array_verify);
          }
        }
        $sales_responses = implode('/', $answer_imploted);
      }else{
        $sales_responses = '';
      }

      $data = array(
        'id_audit'  => $this->request->data['id_audit'],
        'id_dealer' => $this->request->data['id_dealer'],
        'validate'  => '0',
        'status'    => '1',
        'answers'   => $this->request->data['answer'],
        'date'      => $this->request->data['on'],
        'group_id'  => $this->request->data['group_id'],
        'notes'     => $this->request->data['notes'],
        'id_auditor'=> $this->request->data['id_auditor'],
        'sales'     => $sales_responses
        );
      $this->Audit_answer->create();
      if($this->Audit_answer->save($data)){

        $old_answer = $this->Audit_answer->find('first', array(
            'conditions' => array(
            'Audit_answer.id_audit' => $this->request->data['id_audit'],
            'Audit_answer.validate' => 2
            )));

        if($old_answer) {
            $this->Audit_answer->delete($old_answer['Audit_answer']['id']);
        }

        return '{"response": 1}';
      }else{
        return '{"response": 0}';
      }
    }
  }

  public function upload(){
    $this->RequestHandler->respondAs('application/json');
    $terms = explode('/', $this->request->params['form']['name']['type']);
    $admin = new AdminController;
    $new_name = $admin->gen_pass().'.'.$terms['1'];
    if(move_uploaded_file($this->request->params['form']['name']['tmp_name'], WWW_ROOT.'files/uploads/'.$new_name)){
      return '{"name" : "'.$new_name.'"}';
    }else{
      return '{"error": 1}';
    }    
  }

  public function audit_types(){
    $this->RequestHandler->respondAs('application/json');
    $this->loadModel('Audit_type');
    $audit_types = $this->Audit_type->find('all');
    return '{"response": '.json_encode($audit_types).'}';
  }

}
?>