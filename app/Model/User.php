<?php 
App::uses('AuthComponent', 'Controller/Component');
class User extends AppModel {
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required'
            )
        )
    );
    
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }

    public $hasOne = array(
            'Profile' => array(
                'className' => 'Profile',
                'foreignKey' => 'user_id',
                'dependent'    => true
            ),
        );

    public $hasMany = array(
            'Group' => array(
                'className' => 'Groups_user',
                'foreignKey' => 'user_id',
            ),
        );

}
?>