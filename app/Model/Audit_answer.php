<?php 
class Audit_answer extends AppModel {

    public $belongsTo = array(
            'Auditoria' => array(
                'className' => 'Auditoria',
                'foreignKey' => 'id_audit'
            ),
            'Group' => array(
                'className' => 'Group',
            ),
            'Dealer' => array(
                'className' => 'Dealer',
                'foreignKey' => 'id_dealer'
            ),
            'User' => array(
                'className' => 'User',
                'foreignKey' => 'id_auditor'
            ),
    );
}
?>