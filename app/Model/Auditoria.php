<?php 
class Auditoria extends AppModel {
	public $belongsTo = array(
      'Group' => array(
          'className' => 'Group',
          'foreignKey' => 'group_id',
      ),
      'User' => array(
          'className' => 'User',
          'foreignKey' => 'auditor',
      ),
      'Dealer' => array(
          'className' => 'Dealer',
          'foreignKey' => 'dealer',
      ),
  );
}
?>
