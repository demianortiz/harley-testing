<link rel="stylesheet" type="text/css" href="/harley/css/validations.css" />
<div class="title">
  <h5><?php if($au){echo 'Edit Audit';}else{echo 'Add Audit';} ?></h5>
</div>
<form action="" style="position:relative;" class="mainForm audit_add_form" method="POST">
        
  <!-- Input text fields -->
  <fieldset>
    <div class="widget first">
      <div class="head">
        <h5 class="iList"><?php if($au){echo 'Edit Audit';}else{echo 'Add Audit';} ?></h5>
      </div>

      <div class="rowElem noborder">
        <label>Title:</label>
        <div class="formRight">
          <input type="text" name="title" id="title" value="<?php if($au){echo $au['Auditoria']['title'];} ?>"/>
        </div>
        <div class="fix"></div>
          <div id="error-titulo" class="alert tooltip bottom shadow">
            <p><strong>A title is required.</strong></p>
            <p>It’s the way to identify an audit.</p>
          </div>
      </div>

      <div class="rowElem noborder">
        <label>Audit Type:</label>
        <div class="formRight">
          <select multiple name="audit_type[]" id="audit_type">
            <option value="">Please Select</option>
            <?php
            $audits_types = explode(',',$au['Auditoria']['audit_type']);

            foreach ($audit_types as $audit_type){ ?>
              <option value="<?php echo $audit_type['Audit_type']['id']; ?>" 
                  <?php if(in_array($audit_type['Audit_type']['id'], $audits_types)){echo 'selected="selected"';} ?>  >
                <?php echo $audit_type['Audit_type']['name'] ?>
              </option>
            <?php } ?>
          </select>
        </div>
        <div class="fix"></div>
          <div id="error-tipo" class="alert tooltip bottom shadow">
            <p><strong>A type of audit is required.</strong></p>
            <p>Only users who have the type of audit selected will be able to see it.</p>
          </div>
      </div>

      <div class="rowElem">
        <label>Area:</label>
        <div class="formRight area_groups">
          <?php if (!$au): ?>
            <div style="border-bottom:solid 1px #CCCCCC; padding-bottom:5px; margin-bottom:5px;"><b>Remember to check all of the regions if you want to create a Region based audit.</b></div>
          <?php endif ?>
          <?php 
            $name_s = '0';
            $next_s = '1';
            $x=0;
          ?>
          <?php foreach ($times as $llave_t => $time): ?>
          <div class="floatleft">
            <?php if ($au): ?>
                <?php if (array_key_exists($llave_t, $i)): ?>
                  <div class="area-audit"><?php echo $i[$llave_t]['Group']['name'] ?></div>
                <?php else: ?>
                  <br>
                <?php endif ?>
              <?php endif ?>

             <?php
                        $x++;
                  
                   switch($x) {
                    case 1:
                       $class = 'first_select'; break;
                    case 2: 
                       $class = 'second_select'; break;
                    default:
                       $class = '';
                   }
             ?>

            <select name="<?php echo $name_s; ?>" class="get_parent <?php echo $class; ?>" data-next="<?php echo $next_s; ?>"
              data-page="audit"
              multiple="multiple"
              data-lang="Region"
              <?php if ($llave_t != $last_key): ?>
                <?php if (array_key_exists($llave_t, $g)): ?>
                  disabled="disabled"
                <?php endif ?>
              <?php endif ?>
              
            >
              <option value="" disabled selected="selected"></option>
              <?php if (array_key_exists($llave_t, $g)): ?>
                <?php if ($llave_t == $last_key): ?>
                  <?php foreach ($his_groups as $group_h): ?>
                    <option value="<?php echo $group_h['0']['Group']['id']; ?>">
                      <?php echo $group_h['0']['Group']['name']; ?>
                    </option>
                  <?php endforeach ?>
                <?php else: ?>
                  <option value="<?php echo $g[$llave_t]['Group']['id']; ?>" selected="selected">
                    <?php echo $g[$llave_t]['Group']['name']; ?>
                  </option>
                <?php endif ?>
              <?php else: ?>                
                <option value=""></option>             
              <?php endif ?>    
            </select>
                  
          <?php 
            $name_s++;
            $next_s++;
          ?>
          </div>
          <?php endforeach ?>
          <div class="fix"></div>
        </div>
        <div class="fix"></div>
          <div id="error-area" class="alert tooltip bottom shadow">
            <p><strong>You have to select a geographic area for the audit.</strong></p>
            <p>Only users who belong to this geographic area may see it.</p>
          </div>
      </div>
      <div class="rowElem dealer_div_holder">
        <label>Is dealer Related?</label>
        <div class="formRight area_groups">
          <div class="floatleft" style="padding-right:30px;"><input type="checkbox" class="dealer"></div>                        
          <div class="floatleft">
            <div class="dealers_result">
              <?php if ($au): ?>
                <select name="dealer" id="">
                  <option value=""><?php echo $au['Dealer']['name']; ?></option>
                </select>
              <?php else: ?>
                <input type="hidden" name="dealer" value="0">
              <?php endif ?>              
            </div>
          </div> 
          <div class="fix"></div> 
        </div>
        <div class="fix"></div>
          <div id="error-dealer" class="alert tooltip bottom shadow" style="width:450px;">
            <p><strong>If you want to assign this audit to a dealer, you must select it.</strong></p>
            <p>If the dealer you are searching for doesn’t appear, it may be that you’re not in the selected geographic area.</p>
          </div>
      </div>

      <div class="rowElem">
        <div class="groups">
          <?php if ($au): ?>
            <?php $i = 1; ?>
            <?php foreach ($au['Questions'] as $key => $questions): ?>
              <div class="rowElem group_container" data-id="<?php echo $i; ?>">
                <label> Group name:
                  <input type="text" class="group_name"  style="width:75%; float:left"
                  name="group[<?php echo $i; ?>]" 
                  value="<?php echo $key; ?>" 
                  data-id="<?php echo $i; ?>" disabled="disabled">
                    <a href="#" class="delete_group" style="display:inline; margin-top:5px; float:right;" />
                    <img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/trash.png" title="Delete Group" alt="Delete Group">
                    </a>
                    <div class="fix"></div>
                </label>
                <div style="margin-top:70px">
                  <div>
                    <div class="questions">
                      <?php $e = 1; ?>
                      <?php foreach ($questions as $llave => $question): ?>
                        <div class="question_container"  data-group="<?php echo $e; ?>">
                          <input type="hidden" 
                        name="questions[<?php echo $key; ?>][<?php echo $llave; ?>][id]" 
                        value="<?php echo $question['Question']['id']; ?>">
                          <div class="question_holder">
                            <div class="rowElem noborder">
                              <label>Question:</label>
                                <div style="float:right; margin:17px 4px;">
                                <a href="#" class="delete_preg"
                                    data-idq="<?php echo $question['Question']['id']; ?>">
                                    <img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/trash.png" alt="Delete Question" title="Delete Question">
                                </a>
                                </div>
                              <div class="formRight">
                                <input type="text" class="question" 
                                name="questions[<?php echo $key; ?>][<?php echo $llave; ?>][question]" 
                                value="<?php echo $question['Question']['question']; ?>"
                                />
                              </div>
                              <div class="fix"></div>
                                <div class="error-pregunta alert tooltip bottom shadow">
                                    <p><strong>Introduce a question.</strong></p>
                                    <p>You can´t create empty questions.</p>
                                </div>
                            </div>

                            <div class="rowElem ">
                              <div class="floatleft checkbox">
                                <label>Required?</label>
                                <input type="checkbox" class="response_required" 
                                name="questions[<?php echo $key; ?>][<?php echo $llave; ?>][response_required]"
                                value="1"
                                  <?php if ($question['Question']['required'] == '1'): ?>
                                    checked
                                  <?php endif ?>
                                />
                                <div class="fix"></div>
                              </div>
                              <div class="floatleft checkbox">
                                <label>Open?</label>
                                <input type="radio" class="open_response" 
                                name="questions[<?php echo $key; ?>][<?php echo $llave; ?>][open_response]"
                                value="1"
                                  <?php if ($question['Question']['open'] == '1'): ?>
                                  checked
                                  <?php endif ?>
                                />
                                <div class="fix"></div>
                              </div> 
                              <div class="floatleft checkbox">
                                <label>Yes, No, N/A</label>
                                <input type="radio" class="yes_no" 
                                  name="questions[<?php echo $key; ?>][<?php echo $llave; ?>][open_response]" 
                                value="0"
                                <?php if ($question['Question']['yes_no'] == '1'): ?>
                                  checked
                                <?php endif ?>
                                />
                                <div class="fix"></div>
                              </div>
                              <div class="fix"></div>      
                            </div>

                          </div>
                        </div>
                        <?php $e++; ?>
                      <?php endforeach ?>
                    </div> 
                    <div style="padding-top:5px;">
                      <input type="button" value="Add Question" 
                        class="greyishBtn add_question" 
                          data-id="<?php echo $i; ?>" 
                            data-quest="<?php echo $i; ?>"
                              data-updated="<?php echo $e; ?>"
                      />
                    </div>           
                  </div>
                </div>
                <div class="fix"></div>
              </div>
              <?php $i++; ?>
            <?php endforeach ?>            
          <?php endif ?>
        </div>
        <div>
          <input type="button" value="Add Group" class="greyishBtn add_group" 
          data-id="<?php if($au){echo ($i - 1);}else{echo '0';} ?>"
          /> 
        </div>               
      </div>        
        <div id="error-gral-audit" class="alert right shadow">
            <p><strong>To create an audit it´s necessary that it has questions.</strong></p>
            <p>Create some questions and group them up to facilitate the introduction to the user.</p>
        </div>
      <div class="rowElem">



        <?php if ($au): ?>
          <input type="hidden" name="id" value="<?php echo $au['Auditoria']['id'] ?>">
        <?php endif ?>
        <input type="hidden" name="auditor" value="<?php echo $user_id['id']; ?>">
        <input type="hidden" name="post_date" value="<?php echo date('Y-m-d H:i:s'); ?>">
        <input type="hidden" name="status" value="0">
        <input type="hidden" class="first_selected" name="first_selected" id="first_selected" value="<?php if($au){echo $au['Auditoria']['group_id'];}else{echo $user_id['group_id'];} ?>">
        <input type="hidden" class="last_selected" name="group_id" id="last_selected" value="<?php if($au){echo $au['Auditoria']['group_id'];}else{echo $user_id['group_id'];} ?>">
        <input type="submit" value="Save" class="greyishBtn submitForm submitForm_audit" />
        <div class="fix"></div>
      </div>
      <div class="fix"></div>
    </div>
  </fieldset>
</form>
<div class="group" style="display:none;" data-id="0">
  <div class="rowElem group_container">
    <label> Group name:
      <input type="text" class="group_name" style="width:75%; float:left">
      <?php if (!$au): ?>
        <a href="#" class="delete_group" style="display:inline; margin-top:5px; float:right;" />
          <img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/trash.png" title="Delete Group" alt="Delete Group">
        </a>
        <div class="fix"></div>
      <?php endif ?>      
    </label>
    <div style="margin-top:75px;">
      <div>
        <div class="questions"> </div> 
        <div style="padding-top:5px;">
          <input type="button" value="Add Question" class="greyishBtn add_question" data-quest="0"/>
        </div>           
      </div>
    </div>
    <div class="fix"></div>
  </div>
</div>

<div class="question" style="display:none" data-id="0">
  <div class="question_container"  data-group="1">
    <input type="hidden" class="id">
    <div class="question_holder">
      <div class="rowElem noborder">
        <label>Question:</label>
        <div style="float:right; margin:17px 4px;">
            <a href="#" class="delete_preg" data-idq="0"><img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/trash.png" alt="Delete Question" title="Delete Question"></a>
        </div>
        <div class="formRight">
          <input type="text" class="question"/>
        </div>
        <div class="fix"></div>
        <div class="error-pregunta alert tooltip bottom shadow">
            <p><strong>Introduce a question. </strong></p>
            <p>You can´t create empty questions.</p>
        </div>
      </div>

      <div class="rowElem ">
        <div class="floatleft checkbox">
          <label>Required?</label>
          <input type="checkbox" class="response_required" value="1"/>
          <div class="fix"></div>
        </div>
        <div class="floatleft checkbox">
          <label>Open?</label>
          <input type="radio" class="open_response" value="1" checked/>
          <div class="fix"></div>
        </div> 
        <div class="floatleft checkbox">
          <label>Yes, No, N/A</label>
          <input type="radio" class="yes_no" value="0"/>
          <div class="fix"></div>
        </div>
        <div class="fix"></div>      
      </div>

    </div>
  </div>
</div>
