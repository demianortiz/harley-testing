<form action="" class="mainForm" id="valid" method="POST">
        
	<!-- Input text fields -->
  <fieldset>
    <div class="widget first">
      <div class="head">
        <h5 class="iList">Agregar Grupo</h5>
      </div>

      <div class="rowElem">
        <label>Area:</label>
        <div class="formRight area_groups">                        
            <?php if (!$au): ?>
            <div style="border-bottom:solid 1px #CCCCCC; padding-bottom:5px; margin-bottom:5px;"><b>Remember to check all of the regions if you want to create a Region based audit.</b></div>
          <?php endif ?>
          <?php 
            $name_s = '0';
            $next_s = '1';
          ?>
          <?php foreach ($times as $llave_t => $time): ?>
          <div class="floatleft">
            <?php if ($au): ?>
                <?php if (array_key_exists($llave_t, $i)): ?>
                  <?php echo $i[$llave_t]['Group']['name'] ?><br>
                <?php else: ?>
                  <br>
                <?php endif ?>
              <?php endif ?>
            <select name="<?php echo $name_s; ?>" class="get_parent" data-next="<?php echo $next_s; ?>"
              <?php if ($profile['role'] != '3' AND !array_key_exists($llave_t, $g)): ?>
                <?php if (!$au): ?>
                  multiple="multiple"
                <?php endif ?>                
              <?php endif ?>
              data-lang="Region"
              <?php if ($llave_t != $last_key): ?>
                <?php if (array_key_exists($llave_t, $g)): ?>
                  disabled="disabled"
                <?php endif ?>
              <?php endif ?>
              
            >
              <option value="" disabled selected="selected"></option>
              <?php if (array_key_exists($llave_t, $g)): ?>
                <?php if ($llave_t == $last_key): ?>
                  <?php foreach ($his_groups as $group_h): ?>
                    <option value="<?php echo $group_h['0']['Group']['id']; ?>">
                      <?php echo $group_h['0']['Group']['name']; ?>
                    </option>
                  <?php endforeach ?>
                <?php else: ?>
                  <option value="<?php echo $g[$llave_t]['Group']['id']; ?>" selected="selected">
                    <?php echo $g[$llave_t]['Group']['name']; ?>
                  </option>
                <?php endif ?>
              <?php else: ?>                
                <option value=""></option>             
              <?php endif ?>    
            </select>
          <?php 
            $name_s++;
            $next_s++;
          ?>
          </div>
          <?php endforeach ?>
          <div class="fix"></div>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem noborder">
        <label>Nombre:</label>
        <div class="formRight">
          <input type="text" name="name"/>
        </div>
        <div class="fix"></div>
      </div>
      
      <input type="hidden" class="last_selected" name="last_selected" value="">
      <input type="submit" value="Submit form" class="greyishBtn submitForm" />
      <div class="fix"></div>
    </div>
  </fieldset>
</form>