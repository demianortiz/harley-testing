<form action="" class="mainForm" method="POST">
    <fieldset>
        <div class="widget first">

            <div class="head">
                <h5 class="iList">Agregar Banner</h5>
            </div>

            <div class="rowElem">
                <label>Nombre:</label>
                <div class="formRight">
                    <input type="text" name="name" placeholder="Nombre" <?php if (isset($banner)){echo "value='".$banner['Banner']['name']."'";} ?>/>
                </div>
                <div class="fix"></div>
            </div>

            <div class="rowElem">
                <label>Categoria:</label>
                <div class="formRight">
                    <input type="text" name="cat" placeholder="Categoria" <?php if (isset($banner)){echo "value='".$banner['Banner']['cat']."'";} ?>/>
                </div>
                <div class="fix"></div>
            </div>

            <div class="rowElem">
                <label>Orden:</label>
                <div class="formRight">
                    <input type="text" name="order" placeholder="# en el cual este slide estara" <?php if (isset($banner)){echo "value='".$banner['Banner']['order']."'";} ?>/>
                </div>
                <div class="fix"></div>
            </div>

            <div class="rowElem">
                <label>Prefijo Imagen:</label>
                <div class="formRight">
                    <input type="text" name="imgprefix" placeholder="Prefijo de las imagenes del logo y del banner" <?php if (isset($banner)){echo "value='".$banner['Banner']['imgprefix']."'";} ?>/>
                </div>
                <div class="fix"></div>
            </div>

            <div class="rowElem">
                <label>Descripción:</label>
                <div class="formRight">
                    <textarea rows="8" cols="" name="desc"><?php if (isset($banner)){echo $banner['Banner']['desc'];} ?></textarea>
                </div>
                <div class="fix"></div>
            </div>

            <input type="submit" value="Submit form" class="greyishBtn submitForm" />
            <div class="fix"></div>
        </div>
    </fieldset>

    <fieldset>
        <div class="widget">    
            <div class="head"><h5 class="iUpload">Subir Archivos</h5></div>
            <div id="uploader">You browser doesn't have HTML 4 support.</div>                    
        </div>
    </fieldset>
</form>