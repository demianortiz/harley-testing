<div class="title"><h5>Sales Target</h5></div>
<div class="table">
  <div class="head">
    <h5 class="iFrames">Sales Target</h5>
  </div>
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="example2">
    <thead>
      <tr>
        <th>Question</th>
        <th>Audit Type</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($sales as $sale) { ?>
          <tr class="gradeX">
            <td>
            	<a href="<?php echo $this->Html->url( '/', true ); ?>admin/sales_target/<?php echo $sale['Sale']['id']; ?>">
            		<?php echo $sale['Sale']['question']; ?>
            	</a>            	
            </td>
            <td><?php echo $sale['Audit_type']['name']; ?></td>         
          </tr>
      <?php } ?>     
    </tbody>
  </table>
</div>