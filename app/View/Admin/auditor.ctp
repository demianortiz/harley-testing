<link rel="stylesheet" type="text/css" href="/harley/css/validations.css" />
<form action="" style="position:relative;" class="mainForm" method="POST">
        
	<!-- Input text fields -->
  <fieldset>
    <div class="widget first">
      <div class="head">
        <h5 class="iList">Add Auditor</h5>
      </div>

      <div class="rowElem noborder">
        <label>Username:</label>
        <div class="formRight">
          <input type="text" name="username" id="username" <?php if($id){echo 'value="'.$id['User']['username'].'"';} ?>/>
        </div>
        <div class="fix"></div>
          <div id="error-usuario" class="alert tooltip bottom shadow">
            <p><strong>You must have a username.</strong></p>
            <p>It´s the way to identify yourself in the application.</p>
          </div>
          <div id="error-usuario-existe" class="alert tooltip bottom shadow">
            <p><strong>This username already exists.</strong></p>
            <p>Choose another username or check the usernames that have already been created.</p>
          </div>
      </div>

      <div class="rowElem">
        <label>Name:</label>
        <div class="formRight">
          <input type="text" name="name" id="name" <?php if($id){echo 'value="'.$id['Profile']['name'].'"';} ?>/>
        </div>
        <div class="fix"></div>
          <div id="error-nombre" class="alert tooltip bottom shadow">
            <p><strong>You must fill in the name of the auditor.</strong></p>
            <p>It´s the way to identify the user in the application.</p>
          </div>
      </div>

      <div class="rowElem">
        <label>Email:</label>
        <div class="formRight">
          <input type="text" name="email" id="email" <?php if($id){echo 'value="'.$id['Profile']['email'].'"';} ?>/>
        </div>
        <div class="fix"></div>
          <div id="error-email" class="alert tooltip bottom shadow">
            <p><strong>You must introduce the auditor´s email.</strong></p>
            <p>It will be used to send the username and password to login.</p>
          </div>
          <div id="error-email-invalido" class="alert tooltip bottom shadow">
            <p><strong>The email introduced  is not valid.</strong></p>
            <p>Emails look more like: name@domain.com, make sure you put it this way.</p>
          </div>
      </div>

      <div class="rowElem noborder">
        <label>Company:</label>
        <div class="formRight">
          <input type="text" name="company" id="company" <?php if($id){echo 'value="'.$id['Profile']['company'].'"';} ?>/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Password:</label>
        <div class="formRight">
          <div class="floatleft">
            <input type="text" class="password" name="password" id="password" value="">
            <input type="button" value="Generate Password" class="greyishBtn gen_pass floatleft" />
            <div class="floatright" style="padding-left:20px;">Password: <span class="pass"></span></div>
            <div class="fix"></div>
          </div>
          <div class="fix"></div>
        </div>
        <div class="fix"></div>
          <div id="error-password" class="alert tooltip bottom shadow">
            <p><strong>You must create a password for the user.</strong></p>
            <p>You can do it automatically or create it manually.</p>
          </div>
      </div>

      <!--div class="rowElem noborder">
        <label>Language:</label>
        <div class="formRight">
          <select name="lang" id="lang">
            <?php if ($id): ?>
              <option value="spanish" <?php if($id['Profile']['lang'] == 'spanish'){echo "selected";} ?>>Spanish</option>
              <option value="english" <?php if($id['Profile']['lang'] == 'english'){echo "selected";} ?>>English</option>
            <?php else: ?>
              <option value="spanish">Spanish</option>
              <option value="english">English</option>
            <?php endif ?>
          </select>
        </div>
        <div class="fix"></div>
      </div-->

      <div class="rowElem">
        <label>Status:</label>
        <div class="formRight">
          <select name="status" id="status">
            <?php if ($id): ?>
              <option value="0" <?php if($id['User']['status'] == '0'){echo "selected";} ?>>Blocked</option>
              <option value="1" <?php if($id['User']['status'] == '1'){echo "selected";} ?>>Active</option>
            <?php else: ?>
              <option value="0" selected="selected">Blocked</option>
              <option value="1">Active</option>
            <?php endif ?>            
          </select>
        </div>
        <div class="fix"></div>
      </div>

      <table width="100%">
          <tr>
              <td style="border-right: 1px solid #CCC;">
                 <div class="rowElem">
                    <label>Role:</label>
                    <div class="formRight">
                    <select name="role" id="role" class="role">
                        <option value=""></option>
                        <?php if ($id): ?>
                        <?php if ($profile['role'] == '3'): ?>
                            <option value="1" <?php if($id['User']['role'] == '1'){echo "selected";} ?>>Level 1</option>
                            <option value="2" <?php if($id['User']['role'] == '2'){echo "selected";} ?>>Level 2</option>
                        <?php elseif ($profile['role'] == '4'): ?>
                            <option value="1" <?php if($id['User']['role'] == '1'){echo "selected";} ?>>Level 1</option>
                            <option value="2" <?php if($id['User']['role'] == '2'){echo "selected";} ?>>Level 2</option>
                            <option value="3" <?php if($id['User']['role'] == '3'){echo "selected";} ?>>Level 3</option>
                        <?php elseif ($profile['role'] == '6'): ?>
                            <!--option value="6">Admin</option-->
                            <option value="1" <?php if($id['User']['role'] == '1'){echo "selected";} ?>>Level 1</option>
                            <option value="2" <?php if($id['User']['role'] == '2'){echo "selected";} ?>>Level 2</option>
                            <option value="3" <?php if($id['User']['role'] == '3'){echo "selected";} ?>>Level 3</option>
                            <option value="4" <?php if($id['User']['role'] == '4'){echo "selected";} ?>>Level 4</option>
                        <?php endif ?>
                        <?php else: ?>
                        <?php if ($profile['role'] == '3'): ?>
                            <option value="1">Level 1</option>
                            <option value="2">Level 2</option>
                        <?php elseif ($profile['role'] == '4'): ?>
                            <option value="1">Level 1</option>
                            <option value="2">Level 2</option>
                            <option value="3">Level 3</option>
                        <?php elseif ($profile['role'] == '6'): ?>
                            <!--option value="6">Admin</option-->
                            <option value="1">Level 1</option>
                            <option value="2">Level 2</option>
                            <option value="3">Level 3</option>
                            <option value="4">Level 4</option>
                        <?php endif ?>
                        <?php endif ?>
                    </select>
                    </div>
                    <div class="fix"></div>
                    <div id="error-role" class="alert tooltip bottom shadow">
                        <p><strong>Select the auditor´s role.</strong></p>
                        <p>Remember that Level 1 is for dealers and Level 2 is for auditors.</p>
                    </div>
                </div>
              </td>
          </tr>
      </table>

        <div class="rowElem area_select_div">
          <label>Area:</label>
          <div class="formRight area_groups">                    
            <?php if (!$id): ?>
            <div style="border-bottom:solid 1px #CCCCCC; padding-bottom:5px; margin-bottom:5px;"><b>Remember to check all of the regions if you want to create a Region based audit.</b></div>
          <?php endif ?>
          <?php 
            $name_s = '0';
            $next_s = '1';
            $x = 0;
          ?>
          <?php foreach ($times as $llave_t => $time):?>
          <div class="floatleft">
            <?php if ($id): ?>
                <?php if (array_key_exists($llave_t, $i)): ?>
                  <?php echo '<div class="selected_areas">'.$i[$llave_t]['Group']['name'].'</div>'; ?>
                <?php else: ?>
                  <br>
                <?php endif ?>
              <?php endif ?>
            <select name="<?php echo $name_s; ?>" class="get_parent" data-next="<?php echo $next_s; ?>"
              data-page="auditor"
              multiple="multiple"
              data-lang="Region"
              <?php if ($llave_t != $last_key): ?>
                <?php if (array_key_exists($llave_t, $g)): ?>
                  disabled="disabled"
                <?php endif ?>
              <?php endif ?>
              
            >
              <option value="" disabled selected="selected"></option>
              <?php if (array_key_exists($llave_t, $g)): ?>
                <?php if ($llave_t == $last_key): ?>
                  <?php foreach ($his_groups as $group_h): ?>
                    <option value="<?php echo $group_h['0']['Group']['id']; ?>">
                      <?php echo $group_h['0']['Group']['name']; ?>
                    </option>
                  <?php endforeach ?>
                <?php else: ?>
                  <option value="<?php echo $g[$llave_t]['Group']['id']; ?>" selected="selected">
                    <?php echo $g[$llave_t]['Group']['name']; ?>
                  </option>
                <?php endif ?>
              <?php else: ?>                
                <option value=""></option>             
              <?php endif ?>    
            </select>
          <?php 
            $name_s++;
            $next_s++;
            $x++;
          ?>
          </div>
            <div id="error-nivel-<?php echo $x; ?>" class="alert tooltip bottom shadow">
                <p><strong><?php

                    switch($x) {
                        case 1: echo 'You must select a district for the Level 1 auditor.'; break;
                        case 2: echo 'You must select one or more districts for the Level 2 auditor.'; break;
                        case 3: echo 'You must select one or more countries for the Level 3 auditor.'; break;
                        case 4: echo 'You must select a region for the Level 4 auditor.'; break;
                    }


                ?></strong></p>
                <p>The auditor can only consult, fill in or validate audits in his/her geographic area.</p>
            </div>
          <?php endforeach ?>
          <div class="fix"></div>              
          </div>
          <div class="fix"></div>
          <div id="error-area" class="alert tooltip bottom shadow">
            <p><strong>You must select a geographic area for the auditor.</strong></p>
            <p>The auditor can only consult, fill in or validate audits in his/her geographic area.</p>
          </div>
        </div>


        <table width="100%">
          <tr>
            <td>
                <?php if ($id): ?>
                    <div class="rowElem dealer_div dealer_div_holder"
                        <?php if ($id): ?>
                        <?php if ($id['User']['role'] != '1'): ?>
                            style="display:none"
                        <?php endif ?>
                        <?php endif ?>
                    >
                        <label>Dealer:</label>
                        <div class="dealers_result"></div>
                        <div class="formRight">
                        <div class="dealer_select_holder">
                            <?php $dealers = $this->Admin->get_dealers($id['User']['group_id']); ?>
                            <select name="dealer" class="dealer_id">
                            <?php foreach ($dealers as $dealer): ?>
                                <option value="<?php echo $dealer['Dealer']['id']; ?>"
                                <?php if($dealer['Dealer']['id'] == $id['Profile']['dealer']){echo 'selected="selected"';} ?>
                                >
                                <?php echo $dealer['Dealer']['name']; ?>
                                </option>
                            <?php endforeach ?>
                            </select>
                        </div>
                        </div>
                        <div class="fix"></div>
                        <div id="error-dealer" class="alert tooltip bottom shadow">
                            <p><strong>Select the dealer which corresponds to the Level 1 user.</strong></p>
                            <p>Remember that users from Level 1 are dealers that will make audits.</p>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="rowElem dealer_div dealer_div_holder">
                    <label>Dealer:</label>
                    <div class="dealers_result"></div>
                    <div class="formRight">
                        <input type="checkbox" class="dealer_check">
                        <div class="dealer_select_holder"></div>
                    </div>
                    <div class="fix"></div>
                    <div id="error-dealer" class="alert tooltip bottom shadow">
                        <p><strong>Select the dealer which corresponds to the Level 1 user.</strong></p>
                        <p>Remember that users from Level 1 are dealers that will make audits.</p>
                    </div>
                    </div>
                <?php endif ?>
              </td>
          </tr>
        </table>


      <div class="rowElem audit_type">
        <label>Audit Type:</label>
        <div class="formRight">
            <label><input type="checkbox" name="all_types" value="1" id="all_types" /> All types</label>  <br /><br />
          <select name="audit_type[]" <?php if ($id): ?>
        <?php if ($id['User']['role'] == '1' || $id['User']['role'] == '2'): ?>
                  disabled="disabled"
        <?php endif ?>
      <?php endif ?> multiple="multiple" id="audit_type" class="audit_type_select"
          <?php if ($id['User']['role'] == '4' OR $id['User']['role'] == '3'): ?>

          <?php endif ?>
          >
            <?php if ($id): $types = explode(',', $id['Profile']['audit_type']);?>
              <?php foreach ($audit_types as $audit_type){ ?>
                <option value="<?php echo $audit_type['Audit_type']['id'] ?>"
                  <?php if(in_array($audit_type['Audit_type']['id'], $types)){echo "selected";} ?>
                >
                  <?php echo $audit_type['Audit_type']['name'] ?>
                </option>
              <?php } ?>
            <?php else: ?>
              <option value="">Please Select</option>
              <?php foreach ($audit_types as $audit_type){ ?>
                <option <?php if ($id['User']['role'] == '1' || $id['User']['role'] == '2') { echo 'selected="selected"'; } ?> value="<?php echo $audit_type['Audit_type']['id'] ?>">
                  <?php echo $audit_type['Audit_type']['name'] ?>
                </option>
              <?php } ?>
            <?php endif ?>
          </select>
        </div>
        <div class="fix"></div>
          <div id="error-tipo" class="alert tooltip bottom shadow">
            <p><strong>You must introduce a type of audit.</strong></p>
            <p>You will only see the audits of the type selected.</p>
          </div>
      </div>
      
      

      <div class="rowElem noborder">
        <label>Notes:</label>
        <div class="formRight">
          <textarea name="notes" cols="30" rows="10" id="note"><?php if($id){echo $id['Profile']['note'];} ?></textarea>
        </div>
        <div class="fix"></div>
      </div>

        <div id="error-gral" class="alert right shadow">
            <p><strong>The audit can't be saved.</strong></p>
            <p>There are errors in the form. Please check it.</p>
        </div> 
      
      <input type="hidden" class="last_selected" name="last_selected" id="last_selected" value="<?php if($id){echo $id['User']['group_id'];} ?>" data-value="<?php if($id){echo $id['User']['group_id'];} ?>">
      <input type="hidden" id="id" class="id" value="<?php if($id){echo $id['User']['id'];} ?>">
      <input type="hidden" id="id_p" value="<?php if($id){echo $id['Profile']['id'];} ?>">
      <input type="button" value="Save" class="greyishBtn submitForm submit_auditor" />
      <div class="fix"></div>
    </div>
  </fieldset>
</form>