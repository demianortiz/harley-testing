<link rel="stylesheet" type="text/css" href="/harley/css/validations.css" />
<div class="title"><h5>Add Audit Type</h5></div>
<form action="" class="mainForm" method="POST">
        
  <!-- Input text fields -->
  <fieldset>
    <div class="widget first">
      <div class="head">
        <h5 class="iList">Add Audit Type</h5>
      </div>

      <div class="rowElem noborder">
        <label>Name:</label>
        <div class="formRight">
          <input type="text" name="name" id="audit-type-name" value="<?php if($audit_type){echo $audit_type['Audit_type']['name'];} ?>"/>
        </div>
        <div class="fix"></div>
          <div id="error-nombre-tipo" class="alert tooltip bottom shadow">
            <p><strong>Create a descriptive name for this type of audit.</strong></p>
            <p>This will help identify easier when creating auditors or audits.</p>
          </div>
      </div>

      <?php if($audit_type){ ?>
        <input type="hidden" name="id" value="<?php echo $audit_type['Audit_type']['id'] ?>">
      <?php } ?>

      <input type="submit" value="Save" class="submit_audit_type greyishBtn submitForm" />
      <div class="fix"></div>
    </div>
  </fieldset>
</form>