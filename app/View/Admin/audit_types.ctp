<div class="title"><h5>Audit Types</h5></div>
<?php if($this->request->params['pass'] && $this->request->params['pass'][1] == 'success') { ?>
    <div class="alert-ok">The audit type "<?php echo $this->request->params['pass'][2]; ?>" has been <?php if($this->request->params['pass'][0] == 'create') { echo 'created'; } else { echo 'updated'; } ?>.</div>
<?php
}
?>

<?php if($this->request->params['pass'] && $this->request->params['pass'][0] == 'delete') { ?>
    <div class="delete-ok">"<?php echo $this->request->params['pass'][1]; ?>" has been eliminated.</div>
<?php
}
?>
<div class="table">
  <div class="head"><h5 class="iFrames">Audit Types</h5></div>
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
      <tr>
        <th>ID</th>
        <th>Audit Type</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($audit_types as $audit_type) { ?>
        <tr>
          <td><?php echo $audit_type['Audit_type']['id']; ?></td>
          <td>
            <a href="<?php echo Router::url( "/", true ); ?>admin/audit_type/<?php echo $audit_type['Audit_type']['id']; ?>">
              <?php echo $audit_type['Audit_type']['name']; ?>
            </a>
          </td>
          <td class="center">
            <a href="#" class="erase" 
            data-id="<?php echo $audit_type['Audit_type']['id']; ?>" 
            data-url="<?php echo $this->Html->url( '/', true ); ?>admin/audit_types"
            data-name="<?php echo $audit_type['Audit_type']['name']; ?>"
            data-table="Audit_type" 
            data-lang="Do you really want to delete <?php echo $audit_type['Audit_type']['name']; ?>">
              <img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/trash.png" alt="Delete Audit Type" title="Delete Audit Type">
            </a>
          </td>
        </tr>        
      <?php } ?>     
    </tbody>
  </table>
</div>