<div class="title"><h5>Import Sales Target</h5></div>
<form action="" class="mainForm" method="post" enctype="multipart/form-data">
	<fieldset>
		<div class="widget">    
			<div class="head"><h5 class="iRecord">Import</h5></div>  

			<div class="rowElem">
				<label>Insert XLSX file :</label> 
					<div class="formRight">
						<input type="file" name="file" id="file">
					</div>
				<div class="fix"></div>
			</div> 
			<div class="rowElem">
				<label>Download Excel Example:</label> 
					<div class="formRight">
						<a href="<?php echo Router::url( "/", true ); ?>/files/example_sales.xlsx">Example</a>
					</div>
				<div class="fix"></div>
			</div> 

			<input type="submit" value="Import" class="greyishBtn submitForm" />
      <div class="fix"></div>                
		</div>
	</fieldset>
</form>
<?php if (!empty($errors)): ?>
	<?php foreach ($errors as $llave => $errors_values): ?>
		<?php if (!empty($errors_values)): ?>
			<div class="widget">    
				<div class="head"><h5 class="iDenied"><?php echo $llave; ?></h5></div> 
				<?php foreach ($errors_values as $key => $value): ?>
					<div class="rowElem">
						<label><b>Question: <?php echo $key; ?></b></label>
						<?php foreach ($value as $error): ?>
							<div class="formRight">
								<?php echo $error; ?>
							</div>
						<?php endforeach ?>
						<div class="fix"></div>
					</div>
				<?php endforeach ?>
				<div class="fix"></div>                
			</div>
		<?php endif ?>
	<?php endforeach ?>
<?php endif ?>