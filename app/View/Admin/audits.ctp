<div class="title"><h5>Audits</h5></div>

<?php if($this->request->params['pass'] && $this->request->params['pass'][1] == 'success') { ?>
    <div class="alert-ok">The audit "<?php echo $this->request->params['pass'][2]; ?>" has been <?php if($this->request->params['pass'][0] == 'create') { echo 'created'; } else { echo 'updated'; } ?>.</div>
<?php
}
?>

<?php if($this->request->params['pass'] && $this->request->params['pass'][0] == 'delete') { ?>
    <div class="delete-ok">"<?php echo $this->request->params['pass'][1]; ?>" has been eliminated.</div>
<?php
}
?>

<?php if ($profile['role'] != '1'): ?>
<div class="table">
  <div class="pad_10">
    <select class="audit_type_filter_1">
      <option value="">Select Type</option>
      <?php foreach ($audit_types as $type): ?>
        <option value="<?php echo $type['Audit_type']['name']?>"><?php echo $type['Audit_type']['name']?></option>
      <?php endforeach ?>
    </select>
  </div>
  <div class="head"><h5 class="iFrames">Audits To Be Published</h5></div>
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
      <tr>
        <th>Name</th>
        <th>Status</th>
        <th>Auditor</th>
        <th>Area</th>
        <th>Audit Type</th>
        <th>Post Date</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($audits as $audits_group) { ?>
        <?php foreach ($audits_group as $audit) {
            $types = array();
            if (($profile['role'] == '2') && ($audit['Auditoria']['status'] == '0')) { continue; }
            ?>
          <tr class="gradeX">
            <td style="cursor:pointer;" onclick="javascript:window.location.href='<?php echo Router::url( "/", true ); ?>admin/audit/<?php echo $audit['Auditoria']['id']; ?>'">
              <a href="<?php echo Router::url( "/", true ); ?>admin/audit/<?php echo $audit['Auditoria']['id']; ?>">
                <?php echo $audit['Auditoria']['title']; ?>
              </a>              
            </td>
            <td class="center">
                <?php if($audit['Auditoria']['status'] == '0') { ?>
              <a href="#" class="validate_audit" data-id="<?php echo $audit['Auditoria']['id']; ?>" data-action="0" data-lang="Do you want to publish this Audit" data-value="1">
              <img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/upload.png" alt="Publish" title="Publish">
              </a>
                <?php } else { echo 'Published'; } ?>
            </td>
            <td><?php echo $audit['User']['username']; ?></td>
            <td><?php echo $audit['Group']['name']; ?></td>
            <td><?php foreach($audit['Audit_type'] as $type => $val) { array_push($types, $val['Audit_type']['name']);} echo implode(', ', $types); ?></td>
            <td><?php echo $audit['Auditoria']['post_date']; ?></td>
            <td class="center">
              <a href="#" class="erase" 
              data-id="<?php echo $audit['Auditoria']['id']; ?>" 
              data-url="<?php echo Router::url( "/", true ); ?>admin/audits"
              data-name="<?php echo $audit['Auditoria']['title']; ?>" 
              data-table="Auditoriatp" 
              data-lang="Do you really want to delete <?php echo $audit['Auditoria']['title']; ?>">
                <img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/trash.png" alt="Delete" title="Delete">
              </a>
            </td>
          </tr>
        <?php } ?>
      <?php } ?>     
    </tbody>
  </table>
</div>
<?php endif ?>
<form action="<?php echo Router::url( "/", true ); ?>admin/export" class="index_form" method="POST">
  <div class="table">
    <div class="pad_10">

      <select class="audit_type_filter_2">
        <option value="">Select Type</option>
        <?php foreach ($audit_types as $type): ?>
          <option value="<?php echo $type['Audit_type']['name']?>"><?php echo $type['Audit_type']['name']?></option>
        <?php endforeach ?>
      </select>
      <select class="status_filter_2">
        <option value="">Select Status</option>
        <option value="Validated">Validated</option>
        <option value="Completed">Completed</option>
        <option value="Rejected">Rejected</option>
      </select>
      <select class="dealer_filter_2">
        <option value="">Select Dealer</option>
        <?php foreach ($dealers as $dealer): ?>
          <option value="<?php echo $dealer['name']?>"><?php echo $dealer['name']?></option>
        <?php endforeach ?>
      </select>
    </div>
    <div class="head">
      <h5 class="iFrames">
        Latest audits modified /

        <?php if ($profile['role'] != '1') { ?>
            <a href="#" class="index_export"
            data-name="Latest_audits_modified"
            data-lang="Do you want to export Latest audits modified?"
            >
            Export
            <img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/download3.png" alt="">
            </a>
        <?php } ?>
      </h5>
    </div>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example1">
      <thead>
        <tr>
          <th>Name</th>
          <th>Status</th>
          <th>Audit Type</th>
          <th>Area</th>
          <th>Dealer</th>
          <th>Validate</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($validate_audits as $validate_audit) {
                if($profile['role'] == '1' || $profile['role'] == '2') {
                    if($profile['group_id'] != $validate_audit['Dealer']['group_id']) {
                        continue;
                    }
                }
                ?>
            <tr class="gradeX">
              <td style="cursor:pointer;" onclick="javascript:window.location.href='<?php echo Router::url( "/", true ); ?>admin/detail/<?php echo $validate_audit['Audit_answer']['id']; ?>'">
                <a href="<?php echo Router::url( "/", true ); ?>admin/detail/<?php echo $validate_audit['Audit_answer']['id']; ?>">
                  <?php echo $validate_audit['Auditoria']['title']; ?>
                </a>
                <input type="hidden" class="id" name="questions[<?php echo $validate_audit['Auditoria']['title']; ?>][]" value="<?php echo $validate_audit['Audit_answer']['id']; ?>">              
              </td>
              <td>
                <?php 
                if ($validate_audit['Audit_answer']['validate'] == '0'){
                  echo 'Completed';
                }elseif($validate_audit['Audit_answer']['validate'] == '1'){
                  echo 'Validated';
                }elseif($validate_audit['Audit_answer']['validate'] == '3'){
                  echo 'To Validate';
                }else{
                  echo 'Rejected';
                }
                ?>
              </td>
              <td><?php echo $validate_audit['Audit_type']['name']; ?></td>
              <td><?php echo $validate_audit['Group']['name']; ?></td>
              <td><?php echo $validate_audit['Dealer']['name']; ?></td>
              <td class="center">
                <?php if ($profile['role'] != '1' & $profile['role'] != '4' & $profile['role'] != '5'): ?>
                  <?php if(($validate_audit['Audit_answer']['validate'] == '3') && $profile['role'] == '2'): ?>
                  <?php elseif (($profile['role'] == '2') && ($validate_audit['Audit_answer']['validate'] != '1')) : ?>
                    <a href="javascript:void();" class="validate_audit"
                      data-id="<?php echo $validate_audit['Audit_answer']['id']; ?>" 
                      data-action="1" 
                      data-lang="Do you want to change this Audit to 'To Validate'" 
                      data-value="3">
                      <img width="35" src="<?php echo Router::url( "/", true ); ?>img/icon-validate.png" title="Change this Audit to 'To Validate'" alt="Change this Audit to 'To Validate'">
                    </a>
                  <?php elseif(($validate_audit['Audit_answer']['validate'] == '3') || ($validate_audit['Audit_answer']['validate'] == '0')): ?>
                    <a href="javascript:void();" class="validate_audit"
                      data-id="<?php echo $validate_audit['Audit_answer']['id']; ?>" 
                      data-action="1" 
                      data-lang="Do you want to validate this Audit" 
                      data-value="1">
                      <img width="35" src="<?php echo Router::url( "/", true ); ?>img/icon-validate.png" title="Valida esta auditoría" alt="Valida esta auditoría">
                    </a>
                    <a href="javascript:void();" class="validate_audit"
                      data-id="<?php echo $validate_audit['Audit_answer']['id']; ?>" 
                      data-action="1" 
                      data-lang="Do you want to denie this Audit" 
                      data-value="2">
                      <img width="35" src="<?php echo Router::url( "/", true ); ?>img/icon-deny.png" title="Rechaza esta auditoría" alt="Rechaza esta auditoría">
                    </a>
                  <?php endif ?>               
                <?php endif ?>
              </td>
              <td  class="center">
                <?php if ($profile['role'] != '1' & $profile['role'] != '2' & $profile['role'] != '3'): ?>
                  <a href="#" class="erase" 
                  data-id="<?php echo $validate_audit['Audit_answer']['id']; ?>" 
                  data-url="<?php echo Router::url( "/", true ); ?>admin/audits"
                  data-name="<?php echo $validate_audit['Auditoria']['title']; ?>"
                  data-table="Auditoriav" 
                  data-lang="Do you really want to delete <?php echo $validate_audit['Auditoria']['title']; ?>">
                    <img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/trash.png" alt="Delete" title="Delete">
                  </a>
                <?php endif ?>
              </td>
            </tr>
        <?php } ?>     
      </tbody>
    </table>
  </div>
</form>