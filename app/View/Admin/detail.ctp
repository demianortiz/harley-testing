<div class="title">
	<h5><?php echo $audit['Auditoria']['title']; ?><h5>
</div>
<?php if (array_key_exists('User',$profile) && $profile['User']['role'] == '3'): ?>
	<div class="widget first">
	  <div class="head"><h5 class="iImage2">Validation</h5></div>
	  <div class="body aligncenter">
	    <a href="#" title="" class="btn14 mr5 validate_audit" 
	    	data-id="<?php echo $audit['Audit_answer']['id']; ?>" 
	    	data-action="1" 
	    	data-lang="Do you want to validate this Audit" 
	    	data-value="1">
	    	<img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/check.png" alt="">
	    </a>
	    <a href="#" title="" class="btn14 mr5 validate_audit"
	    	data-id="<?php echo $audit['Audit_answer']['id']; ?>" 
	    	data-action="1" 
	    	data-lang="Do you want to denie this Audit" 
	    	data-value="2">
	    	<img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/denied.png" alt="">
	    </a>
	  </div>
	</div>
<?php endif ?>
<br />
<a href="/harley/admin/audits" title="Back" ><<< Back to Audits list</a>

<div class="widget first">
	<div class="head">
  	<h5 class="iInfo">Information</h5>
  </div>
	<div class="body">

		<div class="floatleft w47">
			<div class="list arrow2Blue">
		    <span class="legend">Area</span>
		    <ul>
		      <li><?php echo $audit['Group']['name']; ?></li>
		    </ul>
			</div>

			<div class="list arrow2Blue">
		    <span class="legend">Dealer</span>
		    <ul>
		      <li><?php echo $audit['Dealer']['name']; ?></li>
		    </ul>
			</div>

			<div class="list arrow2Blue">
		    <span class="legend">Creation Date</span>
		    <ul>
		      <li><?php echo $audit['Auditoria']['post_date']; ?></li>
		    </ul>
			</div>

			<?php if ($audit['Auditoria']['sales'] == '1'): ?>
				<div class="list arrow2Blue">
			    <span class="legend">ASO Total</span>
			    <?php 
			    	$aso_questions = explode('/', $audit['Audit_answer']['sales']);
			    	$total_aso = '0';
			    	foreach ($aso_questions as $key => $aso_question) {
			    		$aso_detail = explode(';', $aso_question);
			    		if($aso_detail['1'] == '1'){
			    			$total_aso += '10';
			    		}else{
			    			$total_aso += '0';
			    		}
			    	}
			    ?>
			    <ul>
			      <li><?php echo $total_aso; ?></li>
			    </ul>
				</div>
			<?php endif ?>			
		</div>

		<div class="floatright w47">
			<div class="list arrow2Blue">
		    <span class="legend">Status</span>
		    <ul>
		      <li>
		      	<?php 
            if ($audit['Audit_answer']['validate'] == '0'){
              echo 'Completed';
            }elseif($audit['Audit_answer']['validate'] == '1'){
              echo 'Validated';
            }elseif($audit['Audit_answer']['validate'] == '3'){
              echo 'To Validate';
            }else{
              echo 'Rejected';
            }
            ?>
		      </li>
		    </ul>
			</div>

			<div class="list arrow2Blue">
		    <span class="legend">Validated</span>
		    <ul>
		      <li>
		      	<?php 
		      	if($audit['Audit_answer']['validate'] == '0'){
		      		echo "No";
		      	}else{
		      		echo "Yes";
		      	} 
		      	?>
		      </li>
		    </ul>
			</div>

			<div class="list arrow2Blue">
		    <span class="legend">Posted Date</span>
		    <ul>
		      <li><?php echo $audit['Audit_answer']['date']; ?></li>
		    </ul>
			</div>
		</div>
		<div class="fix"></div>
	</div>
</div>
<?php if ($audit['Auditoria']['sales'] == '1'): ?>
	<?php foreach ($audit['Sales'] as $key => $answerss): ?>
		<div class="widget first">
		  <div class="head">
		  	<h5 class="iDocs"><?php echo $key; ?></h5>
		  </div>
		  <div class="body">
		  	<?php foreach ($answerss as $question): ?>
		  		<div class="rowElem">
			  		<div class="floatleft">
			  			<h4><?php echo $question['Sale']['question']; ?></h4>
						<p>
							<?php if ($question['Answer']['1'] == '0'): ?>
								<img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/denied.png" alt=""> (0) <?php echo $question['Answer']['2'] ?>
							<?php else: ?>
								<img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/check.png" alt=""> (+10) <?php echo $question['Answer']['2'] ?>
							<?php endif ?>
						</p>
			  		</div>
			  		<div class="fix"></div>
		  		</div>
		  	<?php endforeach ?>
		  </div>
		</div>
	<?php endforeach ?>
<?php endif ?>

<?php foreach ($audit['Answers'] as $key => $answers): ?>
	<div class="widget first">
	  <div class="head">
	  	<h5 class="iDocs"><?php echo $key; ?></h5>
	  </div>
	  <div class="body">
	  	<?php foreach ($answers as $question): ?>
	  		<div class="rowElem">
		  		<div class="floatleft">
		  			<h4><?php echo $question['Question']['question']; ?></h4>
						<p><?php echo $question['Answer']['1']; ?></p>
		  		</div>
		  		<div class="floatright">
		  			<?php if (!empty($question['Answer']['2'])) {?>
		  				<img src="<?php echo Router::url( "/", true ); ?>files/uploads/<?php echo $question['Answer']['2']; ?>" alt="">
		  			<?php } ?>		  			
		  		</div>
		  		<div class="fix"></div>
	  		</div>
	  	<?php endforeach ?>
	  </div>
	</div>
<?php endforeach ?>
