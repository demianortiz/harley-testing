<div class="title"><h5>Help</h5></div>
<div class="table">
  <div class="head"><h5 class="iFrames">Help</h5></div>
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
      <tr>
        <th>ID</th>
        <th>Question</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($helps as $help) { ?>
        <tr>
          <td><?php echo $help['Help']['id'] ?></td>
          <td>
            <a href="<?php echo $this->Html->url( '/', true ); ?>admin/help/<?php echo $help['Help']['id'] ?>">
              <?php echo $help['Help']['question'] ?>
            </a>
          </td>
          <td class="center">
            <a href="#" class="erase" 
            data-id="<?php echo $help['Help']['id']; ?>" 
            data-url="<?php echo $this->Html->url( '/', true ); ?>admin/helps" 
            data-table="Help" 
            data-lang="Do you really want to delete <?php echo $help['Help']['question']; ?>">
              <img src="<?php echo $this->Html->url( '/', true ); ?>img/icons/dark/trash.png" alt="Delete" title="Delete">
            </a>
          </td>
        </tr>        
      <?php } ?>     
    </tbody>
  </table>
</div>