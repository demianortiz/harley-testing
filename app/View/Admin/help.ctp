<div class="title"><h5>Add Help</h5></div>
<form action="" class="mainForm" method="POST">
        
  <!-- Input text fields -->
  <fieldset>
    <div class="widget first">
      <div class="head">
        <h5 class="iList">Add Help</h5>
      </div>

      <div class="rowElem noborder">
        <label>Question:</label>
        <div class="formRight">
          <input type="text" name="question" value="<?php if($help){echo $help['Help']['question'];} ?>"/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem noborder">
        <label>Answer:</label>
        <div class="formRight">
          <textarea rows="8" cols="" name="answer"><?php if($help){echo $help['Help']['answer'];} ?></textarea>
        </div>
        <div class="fix"></div>
      </div>

      <?php if($help){ ?>
        <input type="hidden" name="id" value="<?php echo $help['Help']['id'] ?>">
      <?php } ?>
      <input type="submit" value="Save" class="greyishBtn submitForm" />
      <div class="fix"></div>
    </div>
  </fieldset>
</form>