<link rel="stylesheet" type="text/css" href="/harley/css/validations.css" />
<div class="title"><h5>Add Dealer</h5></div>
<div class="answer"></div>
<form action="" class="mainForm" method="POST">
        
	<!-- Input text fields -->
  <fieldset>
    <div class="widget first">
      <div class="head">
        <h5 class="iList">Add Dealer</h5>
      </div>

      <div class="rowElem">
        <label>Name:</label>
        <div class="formRight">
          <input type="text" name="name" id="name" <?php if($id){echo 'value="'.$id['Dealer']['name'].'"';} ?>/>
        </div>
        <div class="fix"></div>
          <div id="error-name" class="alert tooltip bottom shadow">
            <p><strong>You must fill in the name of the dealer.</strong></p>
            <p>It´s the way to identify the dealer in the application.</p>
          </div>
      </div>

      <div class="rowElem">
        <label>Address:</label>
        <div class="formRight">
          <input type="text" name="address" id="address" <?php if($id){echo 'value="'.$id['Dealer']['address'].'"';} ?>/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Manager:</label>
        <div class="formRight">
          <input type="text" name="manager" id="manager" <?php if($id){echo 'value="'.$id['Dealer']['manager'].'"';} ?>/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Phone:</label>
        <div class="formRight">
          <input type="text" name="phone" id="phone" <?php if($id){echo 'value="'.$id['Dealer']['phone'].'"';} ?>/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>ASO:</label>
        <div class="formRight">
          <input type="text" name="size" id="size" <?php if($id){echo 'value="'.$id['Dealer']['size'].'"';} ?>/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Area:</label>
        <div class="formRight area_groups">                        
            <?php if (!$id): ?>
            <div style="border-bottom:solid 1px #CCCCCC; padding-bottom:5px; margin-bottom:5px;"><b>Remember to check all of the regions if you want to create a Region based audit.</b></div>
          <?php endif ?>
          <?php 
            $name_s = '0';
            $next_s = '1';
          ?>
          <?php foreach ($times as $llave_t => $time): ?>
          <div class="floatleft">
            <?php if ($id): ?>
                <?php if (array_key_exists($llave_t, $i)): ?>
                  <?php echo $i[$llave_t]['Group']['name'] ?><br>
                <?php else: ?>
                  <br>
                <?php endif ?>
              <?php endif ?>
            <select name="<?php echo $name_s; ?>" class="get_parent" data-next="<?php echo $next_s; ?>"
              data-page="dealer"
              multiple="multiple"
              data-lang="Region"
              <?php if ($llave_t != $last_key): ?>
                <?php if (array_key_exists($llave_t, $g)): ?>
                  disabled="disabled"
                <?php endif ?>
              <?php endif ?>
              
            >
              <option value="" disabled selected="selected"></option>
              <?php if (array_key_exists($llave_t, $g)): ?>
                <?php if ($llave_t == $last_key): ?>
                  <?php foreach ($his_groups as $group_h): ?>
                    <option value="<?php echo $group_h['0']['Group']['id']; ?>">
                      <?php echo $group_h['0']['Group']['name']; ?>
                    </option>
                  <?php endforeach ?>
                <?php else: ?>
                  <option value="<?php echo $g[$llave_t]['Group']['id']; ?>" selected="selected">
                    <?php echo $g[$llave_t]['Group']['name']; ?>
                  </option>
                <?php endif ?>
              <?php else: ?>                
                <option value=""></option>             
              <?php endif ?>    
            </select>
          <?php 
            $name_s++;
            $next_s++;
          ?>
          </div>
          <?php endforeach ?>
          <div class="fix"></div>
        </div>
        <div class="fix"></div>
      </div>
      
      <input type="hidden" name="id" id="id" <?php if($id){echo 'value="'.$id['Dealer']['id'].'"';} ?>>
      <input type="hidden" class="last_selected" name="last_selected" id="last_selected" <?php if($id){echo 'value="'.$id['Dealer']['group_id'].'"';} ?>>
      <input type="button" value="Save" class="greyishBtn submitForm submit_dealer" />
      <div class="fix"></div>
    </div>
  </fieldset>
</form>