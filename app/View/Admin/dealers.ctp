<div class="title"><h5>Dealers</h5></div>

<?php if($this->request->params['pass'] && $this->request->params['pass'][1] == 'success') { ?>
    <div class="alert-ok">The user "<?php echo $this->request->params['pass'][2]; ?>" has been <?php if($this->request->params['pass'][0] == 'create') { echo 'created'; } else { echo 'updated'; } ?>.</div>
<?php
}
?>

<?php if($this->request->params['pass'] && $this->request->params['pass'][0] == 'delete') { ?>
    <div class="delete-ok">"<?php echo $this->request->params['pass'][1]; ?>" has been eliminated.</div>
<?php
}
?>
<div class="table">
  <div class="head"><h5 class="iFrames">Dealers</h5></div>
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
      <tr>
        <th>Name</th>
        <th>Address</th>
        <th>Area</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($dealers as $dealers_group) { ?>
        <?php foreach ($dealers_group as $dealer) { ?>
          <tr class="gradeX">
            <td>
              <a href="<?php echo Router::url( "/", true ); ?>admin/dealer/<?php echo $dealer['Dealer']['id']; ?>">
              <?php echo $dealer['Dealer']['name']; ?>
              </a>
            </td>
            <td><?php echo $dealer['Dealer']['address']; ?></td>
            <td><?php echo $dealer['Group']['name']; ?></td>
            <td class="center">
              <a href="#" class="erase" 
              data-id="<?php echo $dealer['Dealer']['id']; ?>" 
              data-url="<?php echo Router::url( "/", true ); ?>admin/dealers"
              data-name="<?php echo $dealer['Dealer']['name']; ?>"
              data-table="dealers" 
              data-lang="Do you really want to delete <?php echo $dealer['Dealer']['name']; ?>">
                <img src="<?php echo Router::url( "/", true ); ?>img/icons/dark/trash.png" alt="Delete Dealer" title="Delete Dealer">
              </a>
            </td>
          </tr>
        <?php } ?>
      <?php } ?>     
    </tbody>
  </table>
</div>