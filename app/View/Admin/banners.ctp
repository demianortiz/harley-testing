<div class="table">
    <div class="head"><h5 class="iFrames">Banners</h5></div>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
        <thead>
            <tr>
                <th>Nombre</th>
                <th width="300px">Category</th>
                <th width="100px">Orden</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($banners as $banner) { ?>
                <tr class="gradec">
                    <td>
                        <a href="/admin/addbanner/<?php echo $banner['Banner']['id'] ?>">
                            <?php echo $banner['Banner']['name'] ?>
                        </a>                        
                    </td>
                    <td><?php echo $banner['Banner']['cat'] ?></td>
                    <td class="center"><?php echo $banner['Banner']['order'] ?></td>
                </tr>
            <?php } ?>            
        </tbody>
    </table>
</div>