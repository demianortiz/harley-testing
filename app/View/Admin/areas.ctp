<div class="title"><h5>Areas</h5></div>
<div class="table">
  <form action="<?php echo Router::url( "/", true ); ?>admin/export" class="index_form" method="POST">
    <div class="head">
      <h5 class="iFrames">
        List of Areas
      </h5>
    </div>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example2">
      <thead>
        <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($areas as $area) { ?>
            <tr class="gradeX">
              <td>
                <a href="<?php echo Router::url( "/", true ); ?>admin/area/<?php echo $area['Group']['id']; ?>">
                  <?php echo $area['Group']['name']; ?>
                </a>       
              </td>
            </tr>
        <?php } ?>     
      </tbody>
    </table>
  </form>
</div>