<link rel="stylesheet" type="text/css" href="/harley/css/validations.css" />
<div class="loginWrapper">
  <div class="loginLogo"><?php echo $this->Html->image("loginLogo.png"); ?></div>
    <div class="loginPanel">
        <div class="head"><h5 class="iUser">Login</h5></div>
        <?php echo $this->Form->create('User', array('class' => 'mainForm', 'id' => 'valid')); ?>
            <fieldset>
                <div class="loginRow noborder">
                    <label for="UserUsername">Username:</label>
                    <div class="loginInput"><input type="text" name="data[User][username]" class="validate[required]" id="UserUsername" /></div>
                    <div class="fix"></div>
                </div>
                
                <div class="loginRow">
                    <label for="UserPassword">Password:</label>
                    <div class="loginInput"><input type="password" name="data[User][password]" class="validate[required]" id="UserPassword" /></div>
                    <div class="fix"></div>
                </div>
                
                <div class="loginRow">
                    <input type="submit" value="Enter" class="greyishBtn submitForm" />
                    <div class="fix"></div>
                </div>
            </fieldset>
            <?php if ($this->Session->flash()): ?>
                <div id="error-login" class="alert tooltip right shadow" style="display:block">
                    <p><strong>Invalid login details.</strong></p>
                    <p>Try again and if it doesn’t work or you don’t remember your access information please contact with the administrator of the application.</p>
                </div>
            <?php endif ?>     
        </form>
    </div>
</div>