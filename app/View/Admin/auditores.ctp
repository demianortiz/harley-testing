<div class="title"><h5>Auditors</h5></div>

<?php if($this->request->params['pass'] && $this->request->params['pass'][1] == 'success') { ?>
    <div class="alert-ok">The user "<?php echo $this->request->params['pass'][2]; ?>" has been <?php if($this->request->params['pass'][0] == 'create') { echo 'created'; } else { echo 'updated'; } ?>.</div>
<?php
}
?>

<?php if($this->request->params['pass'] && $this->request->params['pass'][0] == 'delete') { ?>
    <div class="delete-ok">"<?php echo $this->request->params['pass'][1]; ?>" has been eliminated.</div>
<?php
}
?>

<div class="table">
  <div class="head"><h5 class="iFrames">Auditors</h5></div>
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
      <tr>
        <th>Name</th>
        <th>Username</th>
        <th>Role</th>
        <th>Status</th>
        <th>Area</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($users as $users_group) { ?>
            <td>
              <a href="<?php echo $this->Html->url( '/', true ); ?>admin/auditor/<?php echo $users_group['User']['id']; ?>">
                <?php echo $users_group['Profile']['name']; ?>
              </a>              
            </td>
            <td><?php echo $users_group['User']['username']; ?></td>
            <td><?php echo $users_group['User']['role']; ?></td>
            <td>
              <?php 
              if ($users_group['User']['status'] == '1') {
                echo 'Active';
              }else{
                echo "Blocked";
              }
              ?>
            </td>
            <td><?php echo $users_group['Group']['name']; ?></td>
            <td class="center">
              <a href="#" class="erase"
                 data-name="<?php echo $users_group['Profile']['name']; ?>"
              data-id="<?php echo $users_group['User']['id']; ?>" 
              data-url="<?php echo $this->Html->url( '/', true ); ?>admin/auditores" 
              data-table="User" 
              data-lang="Do you really want to delete <?php echo $users_group['Profile']['name']; ?>">
                <img src="<?php echo $this->Html->url( '/', true ); ?>img/icons/dark/trash.png" alt="Delete Auditor" title="Delete Auditor">
              </a>
            </td>
          </tr>
      <?php } ?>     
    </tbody>
  </table>
</div>