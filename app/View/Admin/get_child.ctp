<?php foreach ($childrens as $children): ?>
	<li>
		<a href="#" class="selector_area" 
			data-id="<?php echo $children['Group']['id']; ?>"
			data-next="<?php echo $next; ?>"
		>
			<?php echo $children['Group']['name']; ?>
		</a>
	</li>
<?php endforeach ?>