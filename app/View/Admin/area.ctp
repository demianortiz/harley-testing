<div class="title"><h5>Area</h5></div>
<div class="nNote nInformation hideit">
  <p><strong>INFORMATION: </strong>Once you edit the name, You will have to change the name in the future imports as well.</p>
</div>
<form action="" class="mainForm" id="valid" method="POST">        
  <fieldset>
    <div class="widget first">
      <div class="head">
        <h5 class="iList">Edit Area</h5>
      </div>

      <div class="rowElem">
        <label>Name:</label>
        <div class="formRight area_groups">
        	<input type="text" name="name" value="<?php echo $area['Group']['name']; ?>">
          <div class="fix"></div>
        </div>
        <div class="fix"></div>
      </div>
      
      <input type="hidden" name="id" value="<?php echo $area['Group']['id']; ?>">
      <input type="submit" value="submit Changes" class="greyishBtn submitForm" />
      <div class="fix"></div>
    </div>
  </fieldset>
</form>