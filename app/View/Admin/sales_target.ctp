<div class="title">
  <h5><?php if($sale){echo 'Edit Sale Target';}else{echo 'Add Sale Target';} ?></h5>
</div>
<form action="" class="mainForm" method="POST">
        
  <!-- Input text fields -->
  <fieldset>
    <div class="widget first">
      <div class="head">
        <h5 class="iList"><?php if($sale){echo 'Edit Sale Target';}else{echo 'Add Sale Target';} ?></h5>
      </div>

      <div class="rowElem noborder">
        <label>Question:</label>
        <div class="formRight">
          <input type="text" name="question" id="question" value="<?php if($sale){echo $sale['Sale']['question'];} ?>"/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Audit Type:</label>
        <div class="formRight">
          <select name="audit_type" id="audit_type">
            <option value="">Select Option</option>
            <?php foreach ($audit_types as $key => $type): ?>
              <option value="<?php echo $type['Audit_type']['id']; ?>"
                <?php if($sale){if($sale['Sale']['audit_type'] == $type['Audit_type']['id']){echo 'selected';}} ?>>
                <?php echo $type['Audit_type']['name']; ?>
              </option>
            <?php endforeach ?>
          </select>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Urban:</label>
        <div class="formRight">
          <input type="text" name="urban" id="urban" value="<?php if($sale){echo $sale['Sale']['urban'];} ?>"/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Small:</label>
        <div class="formRight">
          <input type="text" name="small" id="small" value="<?php if($sale){echo $sale['Sale']['small'];} ?>"/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Medium-Small:</label>
        <div class="formRight">
          <input type="text" name="msmall" id="msmall" value="<?php if($sale){echo $sale['Sale']['msmall'];} ?>"/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Medium:</label>
        <div class="formRight">
          <input type="text" name="medium" id="medium" value="<?php if($sale){echo $sale['Sale']['medium'];} ?>"/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Medium-Large:</label>
        <div class="formRight">
          <input type="text" name="mlarge" id="mlarge" value="<?php if($sale){echo $sale['Sale']['mlarge'];} ?>"/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Large:</label>
        <div class="formRight">
          <input type="text" name="large" id="large" value="<?php if($sale){echo $sale['Sale']['large'];} ?>"/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <label>Very-Large:</label>
        <div class="formRight">
          <input type="text" name="vlarge" id="vlarge" value="<?php if($sale){echo $sale['Sale']['vlarge'];} ?>"/>
        </div>
        <div class="fix"></div>
      </div>

      <div class="rowElem">
        <?php if ($sale): ?>
            <input type="hidden" id="id" name="id" value="<?php echo $sale['Sale']['id']; ?>">
          <?php endif ?>
        <input type="submit" value="Save" class="greyishBtn submitForm submit_auditor" />
        <div class="fix"></div>
      </div>
      <div class="fix"></div>
    </div>
  </fieldset>
</form>