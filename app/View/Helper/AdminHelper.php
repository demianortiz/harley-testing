<?php
class AdminHelper extends AppHelper {

	public function get_parents($id = null){
		App::import("Model", "Group");
		$Group = new Group();
    $childrens = $Group->children($id, $recursive = 1);
    return $childrens;
	}
	public function get_dealers($id){
		App::import("Model", "Dealer");
		$Dealer = new Dealer();
    $childrens = $Dealer->find('all', array('conditions' => array('Dealer.group_id' => $id)));
    return $childrens;
	}
}
?>