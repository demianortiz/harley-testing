<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <table style="background-color:#f5f1e4" cellpadding="0" cellspacing="10" width="822">
             <tr>
                <td align="center"><img style="margin-top:100px; display:block; padding:0; border:0;" src="http://173.255.200.14/harley/img/titulo-harley.png" /></td>
            </tr>
            <tr>
                <td align="center" style="padding-top:20px; color:#494539;">¡BIENVENIDO AL GESTOR DE AUDITORÍAS DE HARLEY DAVIDSON!</td>
            </tr>
            <tr>
                <td align="center" style="padding-top:20px; color:#6d654a;">Se ha creado un usuario para que pueda acceder a la aplicación, <br /> a la cual puede <b>acceder desde la siguiente ruta :</b> <a style="color:#f1952e" href="http://173.255.200.14/harley">http://173.255.200.14/harley</a> </td>
            </tr>
            <tr>
                <td align="center" style="color:#6d654a;">
                    <table width="400" cellpadding="10" cellspacing="10">
                        <tr>
                            <td style="border-top:1px solid #ccc; border-bottom:1px solid #ccc" align="center">Usuario: <span style="color:#f1952e"><?php echo $username; ?></span></td>
                        </tr>
                        <tr>
                            <td style="border-top:1px solid #ccc; border-bottom:1px solid #ccc" align="center">Contraseña: <span style="color:#f1952e"><?php echo $password; ?></span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="margin-top:100px; color:#6d654a;">En caso de que tuviera problemas para acceder, puede ponerse en contacto con <br/> el administrador del sistema a través del email <a style="color:#f1952e" href="mailto:info@harley.com">info@harley.com</a></td>
            </tr>
            <tr>
                <td align="center"><img style="margin-top:40px; display:block; padding:0; border:0;" src="http://173.255.200.14/harley/img/footer-harley.jpg" /></td>
            </tr>
        </table>
    </body>
</html>