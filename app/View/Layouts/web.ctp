<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<title>Administrador</title>

<? echo $this->Html->css('main.css');?>
<link href="http://fonts.googleapis.com/css?family=Cuprum" rel="stylesheet" type="text/css" />

<? echo $this->Html->script('jquery-1.4.4.js');?>

<? echo $this->Html->script('spinner/jquery.mousewheel.js');?>
<? echo $this->Html->script('spinner/ui.spinner.js');?>

<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script> -->

<? echo $this->Html->script('fileManager/elfinder.min.js');?>

<? echo $this->Html->script('wysiwyg/jquery.wysiwyg.js');?>
<? echo $this->Html->script('wysiwyg/wysiwyg.image.js');?>
<? echo $this->Html->script('wysiwyg/wysiwyg.link.js');?>
<? echo $this->Html->script('wysiwyg/wysiwyg.table.js');?>

<? echo $this->Html->script('flot/jquery.flot.js');?>
<? echo $this->Html->script('flot/jquery.flot.pie.js');?>
<? echo $this->Html->script('flot/jquery.flot.resize.js');?>
<? echo $this->Html->script('flot/excanvas.min.js');?>

<? echo $this->Html->script('dataTables/jquery.dataTables.js');?>
<? echo $this->Html->script('dataTables/colResizable.min.js');?>

<? echo $this->Html->script('forms/forms.js');?>
<? echo $this->Html->script('forms/autogrowtextarea.js');?>
<? echo $this->Html->script('forms/autotab.js');?>
<? echo $this->Html->script('forms/jquery.validationEngine-en.js');?>
<? echo $this->Html->script('forms/jquery.validationEngine.js');?>
<? echo $this->Html->script('forms/jquery.dualListBox.js');?>
<? echo $this->Html->script('forms/jquery.filestyle.js');?>

<? echo $this->Html->script('colorPicker/colorpicker.js');?>

<? echo $this->Html->script('uploader/plupload.js');?>
<? echo $this->Html->script('uploader/plupload.html5.js');?>
<? echo $this->Html->script('uploader/plupload.html4.js');?>
<? echo $this->Html->script('uploader/jquery.plupload.queue.js');?>

<? echo $this->Html->script('ui/progress.js');?>
<? echo $this->Html->script('ui/jquery.jgrowl.js');?>
<? echo $this->Html->script('ui/jquery.tipsy.js');?>
<? echo $this->Html->script('ui/jquery.alerts.js');?>

<? echo $this->Html->script('jBreadCrumb.1.1.js');?>
<? echo $this->Html->script('cal.min.js');?>
<? echo $this->Html->script('jquery.smartWizard.min.js');?>
<? echo $this->Html->script('jquery.collapsible.min.js');?>
<? echo $this->Html->script('jquery.ToTop.js');?>
<? echo $this->Html->script('jquery.listnav.js');?>
<? echo $this->Html->script('jquery.sourcerer.js');?>
<? echo $this->Html->script('jquery.timeentry.min.js');?>
<? echo $this->Html->script('jquery.prettyPhoto.js');?>

<? echo $this->Html->script('spin.js');?>

<? echo $this->Html->script('custom.js');?>


</head>

<body>

<!-- Top navigation bar -->
<div id="topNav">
    <div class="fixed">
        <div class="wrapper">
            <?php if (empty($controlador)){ ?>
            
            <?php }else{ ?>
              <div class="welcome">
                <a href="#" title="">
                  <?php echo $this->Html->image("userPic.png"); ?>
                </a>
                <span>Welcome, <?php echo $controlador['username']; ?>!</span>
              </div>
            <?php } ?>
            <div class="userNav">
                <ul>
                  <?php if (!empty($controlador)){ ?>
                    <li>
                      <a href="<?php echo $this->Html->url( '/', true ); ?>admin/logout" title="">
                        <?php echo $this->Html->image("icons/topnav/logout.png"); ?>
                        <span>Logout</span>
                      </a>
                    </li>
                  <?php } ?>
                </ul>
            </div>
            <div class="fix"></div>
        </div>
    </div>
</div>

<?php if (!empty($controlador)){ ?>
<!-- Header -->
<div id="header" class="wrapper">
    <div class="logo">
      <a href="/harley" title="">
        <?php echo $this->Html->image("loginLogo.png"); ?>
      </a>
    </div>
    <?php if ($controlador['role'] != '1' & $controlador['role'] != '2'): ?>
      <div class="middleNav">
        <ul>
          <?php if (strstr($this->request->url , 'admin/auditores')){ ?>
            <li class="import"><a href="<?php echo $this->Html->url( '/', true ); ?>admin/auditor_import" title=""><span>Import</span></a></li>
            <li class="add"><a href="<?php echo $this->Html->url( '/', true ); ?>admin/auditor" title=""><span>Add Auditor</span></a></li>             
          <?php }else if(strstr($this->request->url , 'admin/dealers')){ ?>
            <li class="import"><a href="<?php echo $this->Html->url( '/', true ); ?>admin/dealer_import" title=""><span>Import</span></a></li>
            <li class="add"><a href="<?php echo $this->Html->url( '/', true ); ?>admin/dealer" title=""><span>Add Dealer</span></a></li>            
          <?php }else if(strstr($this->request->url , 'admin/audits')){ ?>
            <li class="add"><a href="<?php echo $this->Html->url( '/', true ); ?>admin/audit" title=""><span>Add Audit</span></a></li>
          <?php }else if(strstr($this->request->url , 'admin/helps')){ ?>
            <li class="add"><a href="<?php echo $this->Html->url( '/', true ); ?>admin/help" title=""><span>Add Help</span></a></li>
          <?php }else if(strstr($this->request->url , 'admin/audit_types')){ ?>
            <li class="add"><a href="<?php echo $this->Html->url( '/', true ); ?>admin/audit_type" title=""><span>Add Audit Type</span></a></li>
          <?php }else if(strstr($this->request->url , 'admin/sales_targets')){ ?>
            <li class="import"><a href="<?php echo $this->Html->url( '/', true ); ?>admin/import_sales" title=""><span>Import</span></a></li>
            <li class="add"><a href="<?php echo $this->Html->url( '/', true ); ?>admin/sales_target" title=""><span>Add</span></a></li>
          <?php }else if(strstr($this->request->url , 'admin/areas')){ ?>
            <li class="import">
              <a href="<?php echo $this->Html->url( '/', true ); ?>admin/import_areas" title=""><span>Import</span></a>
            </li>
          <?php } ?>
        </ul>
      </div>
    <?php endif ?>
    
    <div class="fix"></div>
</div>
<!-- Main wrapper -->
<div class="wrapper">
  
  <!-- Left navigation -->

    <div class="leftNav">
      <ul id="menu">
        <li class="dash"><a href="<?php echo $this->Html->url( '/', true ); ?>admin/index" title=""><span>HOME</span></a></li>
        <li class="typo">
          <a href="<?php echo $this->Html->url( '/', true ); ?>admin/audits" title=""><span>AUDITS</span></a>
        </li>
        <?php if ($controlador['role'] != '1' & $controlador['role'] != '2'): ?>
          <li class="typo">
            <a href="#" title="" class="exp"><span>USERS</span></a>
            <ul class="sub">
              <li><a href="<?php echo $this->Html->url( '/', true ); ?>admin/auditores" title="">AUDITORS</a></li>
              <li><a href="<?php echo $this->Html->url( '/', true ); ?>admin/dealers" title="">DEALERS</a></li>
              <li><a href="<?php echo $this->Html->url( '/', true ); ?>admin/areas" title="">AREAS</a></li>
            </ul>
          </li>
          <li class="typo">
            <a href="<?php echo $this->Html->url( '/', true ); ?>admin/audit_types" title=""><span>AUDITS TYPES</span></a>
          </li>
          <li class="help">
            <a href="<?php echo $this->Html->url( '/', true ); ?>admin/helps" title=""><span>HELP</span></a>
          </li>
        <?php endif ?>        
      </ul>
    </div>

  <!-- Content -->  
    <div class="content">
      <?php echo $this->fetch('content'); ?>
    </div>
    <div class="fix"></div>
</div>
<?php }else{ ?>
  <?php echo $this->fetch('content'); ?>
<?php } ?>

<!-- Footer -->
<div id="footer">
  <div class="wrapper">
      <span>© Copyright 2013. All rights reserved.</span>
    </div>
</div>

</body>
</html>
