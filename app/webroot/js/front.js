jQuery(document).ready(function($) {
	$('.hover_top_nav').mouseover(function(){
		$(this).animate({
			color: '#FFFFFF',
		}, 300)
	});
	$('.hover_top_nav').mouseout(function(){
		$(this).animate({
			color: '#9b9b9b',
		}, 300)
	});
	$('.hover_top_nav_red').mouseover(function(){
		$(this).animate({
			color: '#FFFFFF',
		}, 300)
	});
	$('.hover_top_nav_red').mouseout(function(){
		$(this).animate({
			color: '#f76721',
		}, 300)
	});



	/*
	**
	**
	**
	**
	** Menu Ajax Calls **********/
	$(".menu_call").click(function() {

		var id = $(this).attr('data-id');
		var url= "/front/page/"+id;

		$.ajax({
			url: url,
			success: function(data)
			{
				$('.information').html(data);
			}
		});
	});


	/*
	**
	**
	**
	**
	**
	** Slide Show ******/
	// Sets the slides width on page load
	var i = $(window).width();
	if (i > 319){ $('#items > div').css({ width: i }); }

	// Scrollable and navigator plugin settings.
	$("#slider").scrollable({ 
		circular: true, 
		touch: false, 
		easing: 'easeInOutExpo', 
		speed: 1000
	}).autoscroll({ 
	  autoplay: true,
	  autopause:true, 
	  interval: 3000
	});
	
	// Window resize code
	window.api = $("#slider").data("scrollable");
	$(window).resize(function() {
		var a = 1 + window.api.getIndex();
		var w = $(window).width();
		if (w > 319) {
			var l = a * w
			$('#items').css({ left: + - +l });
			$('#items > div').css({ width: w });
		} else {
			$('#items > div').css({ width: 300 });
		}
	});

});