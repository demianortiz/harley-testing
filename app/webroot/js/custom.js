$(function() {

  $('#UserUsername').focus();

  $('.get_parent[name=2]').live('click', function() {
    if($('.get_parent[name=2] :selected').size() >= 2) {
      if($('.role').val() == '3') {
        $('.get_parent[name=3]').attr('disabled','disabled');
      } else {
        $('.get_parent[name=3]').removeAttr('disabled');
      }
    } else {
      
    }
  });

  $('.get_parent[name=1]').live('click', function() {
    if($('.role').val() == '4') {
        $('.get_parent[name=2]').attr('disabled','disabled');
        $('.get_parent[name=3]').attr('disabled','disabled');
      }
  });
  
	//====================Get Parents=================//
	$('.get_parent').live('click', function() {
		var next_id = $(this).attr('data-next');
		var lang_id = $(this).attr('data-lang');
		var page = $(this).attr('data-page');
		var esto = $(this);
		$(this).val() || [];
		if($(this).attr('disabled') != true){
			$.fn.rule($(this).val(), page, esto, next_id, lang_id);

			if($(this).attr('multiple')){
				try
				  {
					if($(this).val().length != '1'){
						$('.dealer_div_holder').hide();			
					}else{
						if(page == 'auditor'){
							if($('.role').val() == '1'){
								$('.dealer_div_holder').show();
							}
						}else{
							$('.dealer_div_holder').show();
						}				
					}
				  }
				catch(err)
				  {
				  //Handle errors here
				  }
			}
		}
	})

	$.fn.rule = function role_rules(val, page, esto, next_id, lang_id){
		var role_select = $('.role').val();
		if(page == 'auditor'){
			if(role_select == '1'){
				if(val.length != '1'){
					jAlert('Please select only one area for user level 1', 'Error');
					esto.find("option").attr("selected", false);
					$('.last_selected').val('');
				}else{
					$.fn.get_parents(next_id, lang_id, val);
					$.fn.get_dealers();
				}
			}if(role_select == '3'){
				if(esto.attr('name') == '1'){
					if(val.length != '1'){
						jAlert('Please select only one Region for user level 3', 'Error');
						esto.find("option").attr("selected", false);
						$('.last_selected').val('');
					}else{
						$.fn.get_parents(next_id, lang_id, val);
					}
				}else{
					$.fn.get_parents(next_id, lang_id, val);
				}
			}if(role_select == '4'){
				if(esto.attr('name') == '1'){
					if(val.length != '1'){
						jAlert('Please select only one area for user level 4', 'Error');
						esto.find("option").attr("selected", false);
						$('.last_selected').val('');
					}else{
						$.fn.get_parents(next_id, lang_id, val);
					}
				}else{
					$.fn.get_parents(next_id, lang_id, val);
				}
			}else{
				$.fn.get_parents(next_id, lang_id, val);
			}
			var str = "";
		  esto.find("option:selected").each(function () {
        str += $(this).text() + " / ";
      });
			$('#note').val(str);
		}else if(page == 'dealer'){
			if(val.length != '1'){
				jAlert('Please select only one area', 'Error');
				esto.find("option").attr("selected", false);
				$('.last_selected').val('');
			}else{
				$.fn.get_parents(next_id, lang_id, val);
			}
		}else{
			$.fn.get_parents(next_id, lang_id, val);
			$.fn.get_dealers();
		}
	}

	$.fn.get_dealers = function get_dealers(){
			$('.dealer_select_holder').show();
			var id = $('.last_selected').val();
        	$.ajax({
			  url: '/harley/admin/get_dealers_info',
			  type: 'POST',
			  data: 'id='+id,
			  complete: function(xhr, textStatus) {
			    //called when complete
			  },
			  success: function(data, textStatus, xhr) {
			    $('.dealer_select_holder').html(data);
			  },
			  error: function(xhr, textStatus, errorThrown) {
			    //called when there is an error
			  }
			});
	}

	$.fn.get_parents = function get_parents(next_id,  lang_id, val){
    
    if(val) {
      $('.last_selected').val(val);
    } else if($('.get_parent[name=3]').val() != null) {
      $('.last_selected').val($('.get_parent[name=3]').val());
    } else if($('.get_parent[name=2]').val() != null) {
      $('.last_selected').val($('.get_parent[name=2]').val());
    } else if($('.get_parent[name=1]').val() != null) {
      $('.last_selected').val($('.get_parent[name=1]').val());
    } else if($('.get_parent[name=0]').val() != null) {
      $('.last_selected').val($('.get_parent[name=0]').val());
    } else {
      $('.last_selected').val($('.first_selected').val());
    }

    if($('.last_selected').val() == '') {
      $('.last_selected').val($('.first_selected').val());
    }

    if(val != null) {
      $.ajax({
        url: '/harley/admin/get_parents',
        type: 'POST',
        data: 'id='+val+'&lang='+lang_id,
        complete: function(xhr, textStatus) {
          //called when complete
        },
        success: function(data, textStatus, xhr) {
          $('.get_parent[name='+next_id+']').html(data);
        },
        error: function(xhr, textStatus, errorThrown) {
          window.location.href = "/harley/admin/";
        }
      });
    }
	}

	$('.selector_area').live('click', function(){
		var id = $(this).attr('data-id');
		var next = $(this).attr('data-next');
		$.ajax({
		  url: '/harley/admin/get_child',
		  type: 'POST',
		  data: 'id='+id+'&next='+next,
		  complete: function(xhr, textStatus) {
		    //called when complete
		  },
		  success: function(data, textStatus, xhr) {
		    $('.data_'+next).empty();
		    $('.data_'+next).html(data);
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    alert('error');
		  }
		});		
	})
	$('.role').live('change', function(){
		//$('.audit_type_select').removeAttr("multiple");
		$('.audit_type').show();
		$('.dealer_div').hide();

    if(this.value == '1') {
      $('.dealer_div').show();
    }

		if(this.value == '3'){
      $('#audit_type').removeAttr("disabled");
      $('#all_types').removeAttr("disabled");
			$('.audit_type_select').attr('multiple', 'multiple');
			$('.dealer_div').hide();

		}else if(this.value == '4'){
      $('#all_types').removeAttr("disabled");
      $('#audit_type').removeAttr("disabled");
			$('.audit_type_select').attr('multiple', 'multiple');
      
		}else if(this.value == '1' || this.value == '2'){
      $('#audit_type').show();
      $('#audit_type').attr("disabled", "disabled");
      $('#all_types').attr('checked','checked');
      $('#all_types').attr('disabled','disabled');
      $('#audit_type option').attr("selected", "selected");
		}else if(this.value == '6'){
			$('.audit_type').hide();
		}
	});

	$('.dealer_check').live('change', function(){
		if($(this).is(':checked')){
			$('.dealer_select_holder').show();
			var id = $('#last_selected').val();
        	$.ajax({
			  url: '/harley/admin/get_dealers_info',
			  type: 'POST',
			  data: 'id='+id,
			  complete: function(xhr, textStatus) {
			    //called when complete
			  },
			  success: function(data, textStatus, xhr) {
			    $('.dealer_select_holder').html(data);
			  },
			  error: function(xhr, textStatus, errorThrown) {
			    //called when there is an error
			  }
			});
	  }else{
	  	$('.dealer_select_holder').hide();
	  }
	});
	

	/*****************Generar Contraseña****************/
	$('.send_pass').attr("disabled", "disabled");
	$('.gen_pass').click(function(){
		$.ajax({
		  url: '/harley/admin/gen_pass',
		  type: 'POST',
		  complete: function(xhr, textStatus) {
		    //called when complete
		  },
		  success: function(data, textStatus, xhr) {
		    $('.pass').html(data);
		    $('.password').val(data);
		    //$('.gen_pass').attr("disabled", "disabled");
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    //called when there is an error
		  }
		});
	})

	/********************Validations******************/
	$('#username').focusout(function(e){
		e.preventDefault();
    if(this.value == '') {
      $('#error-usuario').show();
    } else {
      $('#error-usuario').hide();
      checkUsername(this.value);
    }
  });

	$('#name').focusout(function(e){
		e.preventDefault();
    if(this.value == '') {
      $('#error-nombre').show();
    } else {
      $('#error-nombre').hide();
    }
  });

	$('#password').focusout(function(e){
		e.preventDefault();
    if(this.value == '') {
      $('#error-password').show();
    } else {
      $('#error-password').hide();
    }
  });

	$('#role').focusout(function(e){
		e.preventDefault();
    if( $('select#role option:selected').val() == '') {
      $('#error-role').show();
    } else {
      $('#error-role').hide();
    }
  });

	$('#audit-type').focusout(function(e){
		e.preventDefault();
    if( $('select#audit-type option:selected').val() == '') {
      $('#error-tipo').show();
    } else {
      $('#error-tipo').hide();
    }
  });
  
	$('#email').focusout(function(e){
		e.preventDefault();
    if(this.value == '') {
      $('#error-email').show();
      $('#error-email-invalido').hide();
    } else {
      $('#error-email').hide();
      if(!validateEmail(this.value)) {
        $('#error-email-invalido').show();
        $(this).css('color','#f00');
      } else {
        $('#error-email-invalido').hide();
        $(this).css('color','#000');
      }
    }
  });

	$('#audit-type-name').focusout(function(e){
		e.preventDefault();
    if(this.value == '') {
      $('#error-nombre-tipo').show();
    } else {
      $('#error-nombre-tipo').hide();
    }
  });

	$('#title').focusout(function(e){
		e.preventDefault();
    if(this.value == '') {
      $('#error-titulo').show();
    } else {
      $('#error-titulo').hide();
    }
  });

	$('#title').focusout(function(e){
		e.preventDefault();
    if(this.value == '') {
      $('#error-titulo').show();
    } else {
      $('#error-titulo').hide();
    }
  });

	$('#all_types').click(function(e){
    if(this.checked) {
      //$('#audit_type').attr('disabled', 'disabled');
      $('#audit_type option').attr('selected', 'selected');
    } else {
      $('#audit_type').attr('selectedIndex', '-1').children("option:selected").removeAttr("selected");
    }
  });

  function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if( !emailReg.test( email ) ) {
      return false;
    } else {
      return true;
    }
  }

  function checkUsername(username) {
    string = username;
    $.ajax({
			  url: '/harley/admin/usercheck',
			  type: 'POST',
			  data: 'username='+string,
			  complete: function(xhr, textStatus) {
			    //called when complete
			  },
			  success: function(data, textStatus, xhr) {
			  		if(data == '1') {
              $('#error-usuario-existe').show();
              $('#username').css('color','#f00');
            } else {
              $('#username').css('color','#000');
              $('#error-usuario-existe').hide();
            }
			  },
			  error: function(xhr, textStatus, errorThrown) {
			    //called when there is an error
			  }
			});
  }

	/********************Submiting******************/


  $('.submit_audit_type').click(function(e){
    if($('#audit-type-name').val() == '') {
      e.preventDefault();
      $('#audit-type-name').focus();
      $('#error-nombre-tipo').show();
    } else {
      $('#error-nombre-tipo').hide();
    }
  });

	$('.submit_auditor').click(function(e){
		e.preventDefault();
		var ultimo = $('.last_selected').val();
		var ultimo_atr = $('.last_selected').attr('data-value');
		var error =[];

    if($('#password').val() == '' && $('#id').val() == '') {
        $('#password').focus();
        $('#error-password').show();
    } else {
      $('#error-password').hide();
    }

    if($('#name').val() == '') {
        $('#name').focus();
        $('#error-nombre').show();
    } else {
      $('#error-nombre').hide();
    }

    if($('#email').val() == '') {
      $('#error-email').show();
      $('#error-email-invalido').hide();
    } else {
      $('#error-email').hide();
      if(!validateEmail($("#email").val())) {
        $('#error-email-invalido').show();
        $(this).css('color','#f00');
      } else {
        $('#error-email-invalido').hide();
        $(this).css('color','#000');
      }
    }

    if($('#username').val() == '') {
        $('#username').focus();
        $('#error-usuario').show();
    } else {
      $('#error-usuario').hide();
    }

    if( $('select#role option:selected').val() == '') {
      $('#error-role').show();
    } else {
      $('#error-role').hide();
    }

    if( $('select#role option:selected').val() == '1' && !$('.dealer_check').attr('checked') && $('#id').val() == '' && $('.dealer_id option:selected').val() == '') {
      $('#error-dealer').show();
       $('#error-area').hide();
    } else {
      if($('.dealer_id option:selected').val() == '') {
        $('#error-dealer').show();
         $('#error-area').hide();
      }  else {
        $('#error-dealer').hide();
      }
    }

    if( $('.audit_type_select').val() == null) {
      $('#error-tipo').show();
    } else {
      $('#error-tipo').hide();
    }

    if($('.get_parent[name=0]').val() == '' || $('.get_parent[name=1]').val() == '' || $('.get_parent[name=2]').val() == ''){
          
            //$('#error-area').show();
          
		} else {
         // $('#error-area').hide();
    }

    if($('.selected_areas') && !($('.selected_areas').length > 0)) {
      if ($('.role').val() == '3' || $('.role').val() == '4'){
        if($('.audit_type_select').val() == null){
          $('#error-tipo').show();
        } else {
          $('#error-tipo').hide();
        }
      }

      if ($('.role').val() == '1'){
        if($('.get_parent[name=3]').val() == '' || $('.get_parent[name=3]').val() == null){
          error.push('Please select a Disctrict');
          $('#error-nivel-1').show();
        } else {
          $('#error-nivel-1').hide();
        }
      }else {
        $('#error-nivel-1').hide();
      } if ($('.role').val() == '2'){
        if($('.get_parent[name=3]').val() == '' || $('.get_parent[name=3]').val() == null){
          error.push('Please select a Area');
          $('#error-nivel-2').show();
        } else {
          $('#error-nivel-2').hide();
        }

      }else {
        $('#error-nivel-2').hide();
      } if ($('.role').val() == '3'){
        if($('.audit_type_select').val() == null){
          $('#error-tipo').show();
          error.push('Please select an audit type');
        } else {
          $('#error-tipo').hide();
        }

        if($('.get_parent[name=2]').val() == '' || $('.get_parent[name=2]').val() == null){
          error.push('Please select a Region');
          $('#error-nivel-3').show();
        } else {
          $('#error-nivel-3').hide();
        }
      }else {
        $('#error-nivel-3').hide();
      }
        if ($('#role').val() == '4'){
        if($('.audit_type_select').val() == null){
          $('#error-tipo').show();
          error.push('Please select an audit type');
        } else {
          $('#error-tipo').hide();
        }
        if($('.get_parent[name=1]').val() == '' || $('.get_parent[name=1]').val() == null){
          error.push('Please select a Region');
          $('#error-nivel-4').show();
        } else {
          $('#error-nivel-4').hide();
        }
      } else {
        $('#error-nivel-4').hide();
      }
    }
    
		if(error.length === 0){
			var username = $('#username').val();
			var id = $('#id').val();
			var id_p = $('#id_p').val();
			var name = $('#name').val();
			var email = $('#email').val();
			var password = $('#password').val();
			var last_selected = $('#last_selected').val();
			var role = $('#role').val();
			var audit = $('#audit_type').val() || [];
			var status = $('#status').val();
			var lang = $('#lang').val();
			var note = $('#note').val();
			var company = $('#company').val();
			var dealer_id = $('.dealer_id').val();
			var string = 'username='+username
									+'&id='+id
									+'&id_p='+id_p
									+'&name='+name
									+'&email='+email
									+'&password='+password
									+'&last_selected='+last_selected
									+'&role='+role
									+'&status='+status
									+'&lang='+lang
									+'&note='+note
									+'&company='+company
									+'&audit='+audit
									+'&dealer='+dealer_id;

			$.ajax({
			  url: '/harley/admin/auditor',
			  type: 'POST',
			  data: string,
			  complete: function(xhr, textStatus) {
			    //called when complete
			  },
			  success: function(data, textStatus, xhr) {

          if(id) { action = 'update';} else { action = 'create'; }
			  	if(data == '1'){
            $('#error-area').hide();
            $('#error-dealer').hide();
            $('#error-nivel-1').hide();
            $('#error-nivel-2').hide();
            $('#error-nivel-3').hide();
            $("#error-gral").hide();
			  		//alert('Please check your spam folder, you might get your password there.');
			  		window.location.href ="/harley/admin/auditores/"+action+"/success/"+username;
			  	}else if(data == '2'){
            $('#error-area').hide();
            $('#error-dealer').hide();
            $('#error-nivel-1').hide();
            $('#error-nivel-2').hide();
            $('#error-nivel-3').hide();
            $("#error-gral").hide();
			  		window.location.href ="/harley/admin/auditores/"+action+"/success/"+username;
			  	}else{
				    //$('.answer').html(data);
            $("#error-gral").show();
            console.log(error);
				  }
			  },
			  error: function(xhr, textStatus, errorThrown) {
			    //called when there is an error
			  }
			});
		}else{
			$("#error-gral").show();
		}
	})

  $('#name').focusout(
    function(e) {
      if(this.value == '') {
        $('#error-name').show();
      } else {
        $('#error-name').hide();
      }
    }
  );

	$('.submit_dealer').live('click', function(){
		var name 					= $('#name').val();
		var last_selected = $('#last_selected').val();
		var address 			= $('#address').val();
		var manager 			= $('#manager').val();
		var phone 				= $('#phone').val();
		var size 					= $('#size').val();
		var id 						= $('#id').val();
    var errors = new Array();

    if(name == '') {
      errors.push('Name must be completed');
      $('#error-name').show();
      $('#error-name').focus();
    } else {
      $('#error-name').hide();
    }

    if(!errors.length) {
      var string = 'name='+name
                  +'&last_selected='+last_selected
                  +'&manager='+manager
                  +'&phone='+phone
                  +'&size='+size
                  +'&id='+id
                  +'&address='+address;
      $.ajax({
        url: '/harley/admin/dealer',
        type: 'POST',
        data: string,
        complete: function(xhr, textStatus) {
          //called when complete
        },
        success: function(data, textStatus, xhr) {
          if(id) { action = 'update';} else { action = 'create'; }
          window.location.href='/harley/admin/dealers/'+action+'/success/'+name;
        },
        error: function(xhr, textStatus, errorThrown) {
          //called when there is an error
        }
      });
    }
		
	})

	$('.add_group').live('click', function(){		
		var group_id 		= $(this).attr('data-id');
		var updated_id 	= (Number(group_id) + 1);
		$(this).attr('data-id', updated_id);
		$('.group[data-id="0"] .group_container').attr('data-id', updated_id);

		var group_add = $('.group[data-id="0"]').html();
		$('.groups').append(group_add);
		$('.groups .group_container[data-id='+updated_id+'] input.add_question').attr('data-id', updated_id);
		$('.groups .group_container[data-id='+updated_id+'] input.group_name').attr('name', 'group['+updated_id+']');
		$('.groups .group_container[data-id='+updated_id+'] input.group_name').attr('data-id', updated_id);
	});

	$('.add_question').live('click', function(){
		var updated_qid 	= $(this).attr('data-id');
		var updated_q   	= $(this).attr('data-quest');
		var updated_qs   	= $(this).attr('data-updated');
		var esto 					= $(this);
		var g_name		= $('.groups .group_container[data-id='+updated_qid+'] .group_name[data-id='+updated_qid+']').val();
		if(updated_q == '0'){
			jConfirm('Is "'+g_name+'" the correct group name? (you cannot change it latter).', 'Please confirm', function(r) {
				if(r == true){
					$('.group_name[data-id='+updated_qid +']').attr('disabled', 'disabled');

					var qestion_id		= (Number(updated_q) + 1);
					$('.question[data-id="0"] .question_container').attr('data-group', updated_qid);
					$(esto).attr('data-quest', qestion_id);
					$('.question[data-id="0"] .question_container').attr('data-quest', qestion_id);
					var group_name = $('.groups .group_container[data-id='+updated_qid+'] .group_name[data-id='+updated_qid+']').val();



					$('.question[data-id="0"] .question_container input.question').attr('name', 'questions['+group_name+']['+qestion_id+'][question]');			
					$('.question[data-id="0"] .question_container input.response_required').attr('name', 'questions['+group_name+']['+qestion_id+'][response_required]');			
					$('.question[data-id="0"] .question_container input.open_response').attr('name', 'questions['+group_name+']['+qestion_id+'][open_response]');			
					$('.question[data-id="0"] .question_container input.yes_no').attr('name', 'questions['+group_name+']['+qestion_id+'][open_response]');			
					$('.question[data-id="0"] .question_container input.id').attr('name', 'questions['+group_name+']['+qestion_id+'][id]');			
							

					var question_add 	= $('.question[data-id="0"]').html();			
					$('.groups .group_container[data-id='+updated_qid+'] .questions').append(question_add);
				}
			});	
		}else{
			var qestion_id		= (Number(updated_q) + 1);
			$('.question[data-id="0"] .question_container').attr('data-group', updated_qid);
			$(esto).attr('data-quest', qestion_id);
			$('.question[data-id="0"] .question_container').attr('data-quest', qestion_id);
			var group_name = $('.groups .group_container[data-id='+updated_qid+'] .group_name[data-id='+updated_qid+']').val();



			$('.question[data-id="0"] .question_container input.question').attr('name', 'questions['+group_name+']['+qestion_id+'][question]');			
			$('.question[data-id="0"] .question_container input.response_required').attr('name', 'questions['+group_name+']['+qestion_id+'][response_required]');			
			$('.question[data-id="0"] .question_container input.open_response').attr('name', 'questions['+group_name+']['+qestion_id+'][open_response]');			
			$('.question[data-id="0"] .question_container input.yes_no').attr('name', 'questions['+group_name+']['+qestion_id+'][open_response]');			
			$('.question[data-id="0"] .question_container input.id').attr('name', 'questions['+group_name+']['+qestion_id+'][id]');			
					

			var question_add 	= $('.question[data-id="0"]').html();			
			$('.groups .group_container[data-id='+updated_qid+'] .questions').append(question_add);
		}
				
	});

	$('.delete_preg').live('click', function(e){
		e.preventDefault();
		var esto = $(this);
		var question_id 	= $(this).attr('data-idq');
		jConfirm('Do you really want to delete this question?', 'Please confirm', function(r) {
			if(r == true){
				if(question_id != '0'){
					$.ajax({
					  url: '/harley/admin/rmv_question',
					  type: 'POST',
					  data: 'id='+question_id,
					  complete: function(xhr, textStatus) {
					    //called when complete
					  },
					  success: function(data, textStatus, xhr) {
					    
					  },
					  error: function(xhr, textStatus, errorThrown) {
					    //called when there is an error
					  }
					});
				}
				$(esto).parent().parent().parent().remove();
			}
		});		
	});
	$('.delete_group').live('click', function(e){
		e.preventDefault();
		var esto = $(this);
		jConfirm('Do you really want to delete this group?', 'Please confirm', function(r) {
			if(r == true){
				$(esto).parent().parent().remove();
			}
		});		
	});

	$('.dealer').live('click', function(){
		if($(this).attr('checked') == true){
			$('.dealers_result').show();
			var dealer_id = $('.last_selected').val();
			$.ajax({
			  url: '/harley/admin/get_dealers',
			  type: 'POST',
			  data: 'id='+dealer_id,
			  complete: function(xhr, textStatus) {
			    //called when complete
			  },
			  success: function(data, textStatus, xhr) {
			    $('.dealers_result').html(data);
			  },
			  error: function(xhr, textStatus, errorThrown) {
			    //called when there is an error
			  }
			});			
		}else{
			$('.dealers_result').hide();
		}
	})


  /************************* Exports ********************************/
	/*$('.export').live('click', function(){
		var name = $(this).attr('data-name');
		var lang = $(this).attr('data-lang');
		jConfirm(lang, 'Please confirm', function(r) {
			if(r == true){
				$.ajax({
				  url: '/harley/admin/export',
				  type: 'POST',
				  data: 'name='+name,
				  complete: function(xhr, textStatus) {
				    //called when complete
				  },
				  success: function(data, textStatus, xhr) {
				     window.location.href = '/admin/export';
				  },
				  error: function(xhr, textStatus, errorThrown) {
				    //called when there is an error
				  }
				});
			}
		});				
	});*/




 //================= Submitting validations =======================//
	$('.validate_audit').click(function(){
		var id = $(this).attr('data-id');
		var ac = $(this).attr('data-action');
		var va = $(this).attr('data-value');
		var lang = $(this).attr('data-lang');
		jConfirm(lang, 'Please confirm', function(r) {
			if(r == true){
				$.ajax({
				  url: '/harley/admin/validate_audit',
				  type: 'POST',
				  data: 'id='+id+'&action='+ac+'&value='+va,
				  complete: function(xhr, textStatus) {
				    //called when complete
				  },
				  success: function(data, textStatus, xhr) {
				    window.location.href = "/harley/admin/audits";
				  },
				  error: function(xhr, textStatus, errorThrown) {
				    //called when there is an error
				  }
				});
			}
		});		
	});


	//========================== Erase Ajax ===============================//
	$('.erase').click(function(){
		var id 		= $(this).attr('data-id');
		var url 	= $(this).attr('data-url');
    var name 	= $(this).attr('data-name');
		var table = $(this).attr('data-table');
		var lang 	= $(this).attr('data-lang');
		jConfirm(lang, 'Please confirm', function(r) {
			if(r == true){
				$.ajax({
				  url: '/harley/admin/delete',
				  type: 'POST',
				  data: 'id='+id+'&url='+url+'&table='+table,
				  complete: function(xhr, textStatus) {
				    //called when complete
				  },
				  success: function(data, textStatus, xhr) {
				    window.location.href = url+'/delete/'+name;
				  },
				  error: function(xhr, textStatus, errorThrown) {
				    //called when there is an error
				  }
				});
			}
		});
		
	});

	//========================== submit audit==============================//
	$('.add_group').trigger('click');
	$('.submitForm_audit').live('click', function() {
		var error =[];
		if (!$.trim( $('.questions').html() ).length){
			error.push('There is a group empty please fill in with questions. ');
      $('#error-gral-audit').show();
		} else {
      $('#error-gral-audit').hide();
    }

  $('.question_container input.question').each(function(index,currentElement) {
      if(index < $('.question_container input.question').length - 1) {
        if ( ($(currentElement).val() == '')) {
            error.push('There is a group empty please fill in with questions. ');
            
            $('.error-pregunta').each(function(index2,currentElement2) {
              if(index == index2) {
                $(currentElement2).show();
              } else {
                $(currentElement2).hide();
              }
            });
        }
      } else if($('#id').val() != '') {
        if($(currentElement).val() == '' && index == 0) {
           error.push('There is a group empty please fill in with questions. ');
           $('#error-gral-audit').show();
        } else {
          $('#error-gral-audit').hide();
        }
      }
  });

    if(($('.get_parent:first').val() == null)) {
      error.push('Please select an Area. ');
      $('#error-area').show();
    } else if(($('.get_parent:first').val() == '') && $('.area-audit').length == 0){
			error.push('Please select an Area. ');
      $('#error-area').show();
		} else {
      $('#error-area').hide();
    }


		if($('.dealer').attr('checked') == true){
			if($('.dealers_result select').val() == ''){
				error.push('Please select a Dealer. ');
        $('#error-dealer').show();
			} else {
        $('#error-dealer').hide();
      }
		}
    if($('#title').val() == ''){
			error.push('Please select an Audit title. ');
      $('#error-titulo').show();
      $('#error-titulo').focus();
		} else {
      $('#error-titulo').hide();
    }

		if($('#audit_type').val() == null){
			error.push('Please select an Audit Type. ');
      $('#error-tipo').show();
		} else {
      $('#error-tipo').hide();
    }
		if(error.length === 0){
      $('#error-gral-audit').hide();
			$('.audit_add_form').submit();
		}else{
			//alert(error);
      //$('#error-gral-audit').show();
		}
		return false;
	});


	
	//===== PrettyPhoto lightbox plugin =====//
	
	$("a[rel^='prettyPhoto']").prettyPhoto();


	//===== Custom single file input =====//
	
	$("input.fileInput").filestyle({ 
	imageheight : 26,
	imagewidth : 89,
	width : 296
	});
	
	
	//===== Dual select boxes =====//
	
	$.configureBoxes();
	
	
	//===== Time picker =====//
	
	$('.timepicker').timeEntry({
		show24Hours: true, // 24 hours format
		showSeconds: true, // Show seconds?
		spinnerImage: '/front/img/ui/spinnerUpDown.png', // Arrows image
		spinnerSize: [17, 26, 0], // Image size
		spinnerIncDecOnly: true // Only up and down arrows
		});


	//===== Wizard =====//

	$('.wizard').smartWizard({
		selected: 0,  // Selected Step, 0 = first step   
		keyNavigation: true, // Enable/Disable key navigation(left and right keys are used if enabled)
		enableAllSteps: false,  // Enable/Disable all steps on first load
		transitionEffect: 'slideleft', // Effect on navigation, none/fade/slide/slideleft
		contentURL:null, // specifying content url enables ajax content loading
		contentCache:true, // cache step contents, if false content is fetched always from ajax url
		cycleSteps: false, // cycle step navigation
		enableFinishButton: false, // makes finish button enabled always
		errorSteps:[],    // array of step numbers to highlighting as error steps
		labelNext:'Next', // label for Next button
		labelPrevious:'Previous', // label for Previous button
		labelFinish:'Finish',  // label for Finish button        
	  // Events
		onLeaveStep: null, // triggers when leaving a step
		onShowStep: null,  // triggers when showing a step
		onFinish: null  // triggers when Finish button is clicked
	 });

	
	//===== File uploader =====//
	
	$("#uploader").pluploadQueue({
		runtimes : 'html5,html4',
		url : '/admin/uploader',
		max_file_size : '2mb',
		unique_names : false,
		filters : [
			{title : "Image files", extensions : "jpg,gif,png"},
			{title : "Zip files", extensions : "zip"}
		]
	});
	


	//===== File manager =====//	
	
	$('#fileManager').elfinder({
		url : 'php/connector.php',
	})
	

	//===== Alert windows =====//

	$(".bAlert").click( function() {
		jAlert('This is a custom alert box. Title and this text can be easily editted', 'Alert Dialog Sample');
	});
	
	$(".bConfirm").click( function() {
		jConfirm('Can you confirm this?', 'Confirmation Dialog', function(r) {
			jAlert('Confirmed: ' + r, 'Confirmation Results');
		});
	});
	
	$(".bPromt").click( function() {
		jPrompt('Type something:', 'Prefilled value', 'Prompt Dialog', function(r) {
			if( r ) alert('You entered ' + r);
		});
	});
	
	$(".bHtml").click( function() {
		jAlert('You can use HTML, such as <strong>bold</strong>, <em>italics</em>, and <u>underline</u>!');
	});


	//===== Accordion =====//		
	
	$('div.menu_body:eq(0)').show();
	$('.acc .head:eq(0)').show().css({color:"#2B6893"});
	
	$(".acc .head").click(function() {	
		$(this).css({color:"#2B6893"}).next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
		$(this).siblings().css({color:"#404040"});
	});
	
	
	
	
	//===== WYSIWYG editor =====//
	var value_of_textarea = $('.addcalltext').val();
	$('.wysiwyg').wysiwyg({
		iFrameClass: "wysiwyg-input",
		initialContent: function() {
                    return value_of_textarea;
                },
		controls: {
			bold          : { visible : true },
			italic        : { visible : true },
			underline     : { visible : true },
			strikeThrough : { visible : false },
			
			justifyLeft   : { visible : true },
			justifyCenter : { visible : true },
			justifyRight  : { visible : true },
			justifyFull   : { visible : true },
			
			indent  : { visible : true },
			outdent : { visible : true },
			
			subscript   : { visible : false },
			superscript : { visible : false },
			
			undo : { visible : true },
			redo : { visible : true },
			
			insertOrderedList    : { visible : true },
			insertUnorderedList  : { visible : true },
			insertHorizontalRule : { visible : false },

			h1: {
			visible: true,
			className: 'h1',
			command: ($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
			arguments: ($.browser.msie || $.browser.safari) ? '<h1>' : 'h1',
			tags: ['h1'],
			tooltip: 'Header 1'
			},
			h2: {
			visible: true,
			className: 'h2',
			command: ($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
			arguments: ($.browser.msie || $.browser.safari) ? '<h2>' : 'h2',
			tags: ['h2'],
			tooltip: 'Header 2'
			},
			h3: {
			visible: true,
			className: 'h3',
			command: ($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
			arguments: ($.browser.msie || $.browser.safari) ? '<h3>' : 'h3',
			tags: ['h3'],
			tooltip: 'Header 3'
			},
			h4: {
			visible: true,
			className: 'h4',
			command: ($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
			arguments: ($.browser.msie || $.browser.safari) ? '<h4>' : 'h4',
			tags: ['h4'],
			tooltip: 'Header 4'
			},
			h5: {
			visible: true,
			className: 'h5',
			command: ($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
			arguments: ($.browser.msie || $.browser.safari) ? '<h5>' : 'h5',
			tags: ['h5'],
			tooltip: 'Header 5'
			},
			h6: {
			visible: true,
			className: 'h6',
			command: ($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
			arguments: ($.browser.msie || $.browser.safari) ? '<h6>' : 'h6',
			tags: ['h6'],
			tooltip: 'Header 6'
			},
			
			cut   : { visible : true },
			copy  : { visible : true },
			paste : { visible : true },
			html  : { visible: true },
			increaseFontSize : { visible : false },
			decreaseFontSize : { visible : false },
			},
		events: {
			click: function(event) {
				if ($("#click-inform:checked").length > 0) {
					event.preventDefault();
					alert("You have clicked jWysiwyg content!");
				}
			}
		}
	});
	
	//$('.wysiwyg').wysiwyg("insertHtml", "Sample code");







	
	//===== ToTop =====//

	$().UItoTop({ easingType: 'easeOutQuart' });	
	
	
	//===== Spinner options =====//
	
	var itemList = [
		{url: "http://ejohn.org", title: "John Resig"},
		{url: "http://bassistance.de/", title: "J&ouml;rn Zaefferer"},
		{url: "http://snook.ca/jonathan/", title: "Jonathan Snook"},
		{url: "http://rdworth.org/", title: "Richard Worth"},
		{url: "http://www.paulbakaus.com/", title: "Paul Bakaus"},
		{url: "http://www.yehudakatz.com/", title: "Yehuda Katz"},
		{url: "http://www.azarask.in/", title: "Aza Raskin"},
		{url: "http://www.karlswedberg.com/", title: "Karl Swedberg"},
		{url: "http://scottjehl.com/", title: "Scott Jehl"},
		{url: "http://jdsharp.us/", title: "Jonathan Sharp"},
		{url: "http://www.kevinhoyt.org/", title: "Kevin Hoyt"},
		{url: "http://www.codylindley.com/", title: "Cody Lindley"},
		{url: "http://malsup.com/jquery/", title: "Mike Alsup"}
	];

	var opts = {
		's1': {decimals:2},
		's2': {stepping: 0.25},
		's3': {currency: '$'},
		's4': {},
		's5': {
			//
			// Two methods of adding external items to the spinner
			//
			// method 1: on initalisation call the add method directly and format html manually
			init: function(e, ui) {
				for (var i=0; i<itemList.length; i++) {
					ui.add('<a href="'+ itemList[i].url +'" target="_blank">'+ itemList[i].title +'</a>');
				}
			},

			// method 2: use the format and items options in combination
			format: '<a href="%(url)" target="_blank">%(title)</a>',
			items: itemList
		}
	};

	for (var n in opts)
		$("#"+n).spinner(opts[n]);

	$("button").click(function(e){
		var ns = $(this).attr('id').match(/(s\d)\-(\w+)$/);
		if (ns != null)
			$('#'+ns[1]).spinner( (ns[2] == 'create') ? opts[ns[1]] : ns[2]);
	});


	//===== Contacts list =====//
	
	$('#myList').listnav({ 
		initLetter: 'all', 
		includeAll: true, 
		includeOther: true, 
		flagDisabled: true, 
		noMatchText: 'No hay nada que coincida con tu filtro, por favor haga clic en una letra.', 
		prefixes: ['the','a'] ,
	});


	//===== ShowCode plugin for <pre> tag =====//

	$('.showCode').sourcerer('js html css php'); // Display all languages
	$('.showCodeJS').sourcerer('js'); // Display JS only
	$('.showCodeHTML').sourcerer('html'); // Display HTML only
	$('.showCodePHP').sourcerer('php'); // Display PHP only
	$('.showCodeCSS').sourcerer('css'); // Display CSS only


	//===== Calendar =====//

	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next',
			center: 'title',
			right: 'month,basicWeek,basicDay'
		},
		editable: true,
		events: {
	        url: '/front/calendar/get',
	        type: 'POST',
	        data: {
	            start: 'start',
	            end: 'end'
	        },
	        error: function() {
	            alert('there was an error while fetching events!');
	        }
	    }
	});

	$('#admincalendar').fullCalendar({
		header: {
			left: 'prev,next',
			center: 'title',
			right: 'month,basicWeek,basicDay'
		},
		editable: true,
		events: {
	        url: '/front/centro/getcal',
	        type: 'POST',
	        data: {
	            start: 'start',
	            end: 'end'
	        },
	        error: function() {
	            alert('Emos tenido problemas para obtener los eventos!');
	        }
	    }
	});
	
	
	//===== Dynamic data table =====//

	Modified = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sDom": '<""f>t<"F"lp>'
	});

	ToBePublished = $('#example1').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sDom": '<""f>t<"F"lp>'
	});

	Index = $('#example2').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sDom": '<""f>t<"F"lp>'
	});
	$('.index_export').live('click', function() {
		$('.index_form').submit();
    } );

	$('.audit_type_filter_3').live('change', function(){
		Index.fnFilter( this.value, 2 );
	});
	$('.status_filter_3').live('change', function(){
		Index.fnFilter( this.value, 1 );
	});
	$('.dealer_filter_3').live('change', function(){
		Index.fnFilter( this.value);
	});

	$('.audit_type_filter_2').live('change', function(){
		ToBePublished.fnFilter( this.value, 2 );
	});
	$('.status_filter_2').live('change', function(){
		ToBePublished.fnFilter( this.value, 1 );
	});
	$('.dealer_filter_2').live('change', function(){
		ToBePublished.fnFilter( this.value);
	});

	$('.region_filter_1').live('keypress', function(){
		Index.fnFilter( this.value, 2 );
	});
	$('.worldzone_filter_1').live('keypress', function(){
		Index.fnFilter( this.value, 2 );
	});
	$('.distrito_filter_1').live('keypress', function(){
		Index.fnFilter( this.value, 2 );
	});
	$('.audit_type_filter_1').live('change', function(){
		Modified.fnFilter( this.value, 4 );
	});

	$('.region_filter_3').live('keypress', function(){
		Index.fnFilter( this.value, 2 );
	});
	$('.worldzone_filter_3').live('keypress', function(){
		Index.fnFilter( this.value, 2 );
	});
	$('.distrito_filter_3').live('keypress', function(){
		Index.fnFilter( this.value, 2 );
	});
	$('.status_filter_3').live('change', function(){
		Index.fnFilter( this.value, 1 );
	});
	
	//===== Form elements styling =====//
	
	$('.form_preatty').jqTransform({imgPath:'/front/img/forms'});	
	
	//===== Form validation engine =====//

	$("#valid").validationEngine();
	

	//===== Datepickers =====//

	/*$( ".datepicker" ).datepicker({ 
		defaultDate: +7,
		autoSize: true,
		appendText: '(dd-mm-yyyy)',
		dateFormat: 'dd-mm-yy',
	});	

	$( ".datepickerInline" ).datepicker({ 
		defaultDate: +7,
		autoSize: true,
		appendText: '(dd-mm-yyyy)',
		dateFormat: 'dd-mm-yy',
		numberOfMonths: 1
	});	*/


	//===== Progressbar (Jquery UI) =====//

	/*$( "#progressbar" ).progressbar({
			value: 37
	});*/
		
		
	//===== Tooltip =====//
		
	$('.leftDir').tipsy({fade: true, gravity: 'e'});
	$('.rightDir').tipsy({fade: true, gravity: 'w'});
	$('.topDir').tipsy({fade: true, gravity: 's'});
	$('.botDir').tipsy({fade: true, gravity: 'n'});

		
	//===== Information boxes =====//
		
	$(".hideit").click(function() {
		$(this).fadeOut(400);
	});
	

	//=====Resizable table columns =====//
	
	var onSampleResized = function(e){
		var columns = $(e.currentTarget).find("th");
		var msg = "columns widths: ";
		columns.each(function(){ msg += $(this).width() + "px; "; })
	};	

	$(".resize").colResizable({
		liveDrag:true, 
		gripInnerHtml:"<div class='grip'></div>", 
		draggingClass:"dragging", 
		onResize:onSampleResized});


	//===== Left navigation submenu animation =====//	
		
	$("ul.sub li a").hover(function() {
	$(this).stop().animate({ color: "#3a6fa5" }, 400);
	},function() {
	$(this).stop().animate({ color: "#494949" }, 400);
	});
	
	
	//===== Image gallery control buttons =====//

	 $(".pics ul li").hover(
		  function() { $(this).children(".actions").show("fade", 200); },
		  function() { $(this).children(".actions").hide("fade", 200); }
	 );
	

	//===== Color picker =====//

	$('#color').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});
	$('#color1').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});	
	$('#color2').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});
	
	
	//===== Autogrowing textarea =====//
	
	$(".auto").autoGrow();
	

	//===== Autotabs. Inline data rows =====//

	$('.onlyNums input').autotab_magic().autotab_filter('numeric');
	$('.onlyText input').autotab_magic().autotab_filter('text');
	$('.onlyAlpha input').autotab_magic().autotab_filter('alpha');
	$('.onlyRegex input').autotab_magic().autotab_filter({ format: 'custom', pattern: '[^0-9\.]' });
	$('.allUpper input').autotab_magic().autotab_filter({ format: 'alphanumeric', uppercase: true });
	
	
	//===== jQuery UI sliders =====//	
	
	/*$( ".uiSlider" ).slider();
	
	$( ".uiSliderInc" ).slider({
		value:100,
		min: 0,
		max: 500,
		step: 50,
		slide: function( event, ui ) {
			$( "#amount" ).val( "$" + ui.value );
		}
	});
	$( "#amount" ).val( "$" + $( ".uiSliderInc" ).slider( "value" ) );
		
	$( ".uiRangeSlider" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 75, 300 ],
		slide: function( event, ui ) {
			$( "#rangeAmount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		}
	});
	$( "#rangeAmount" ).val( "$" + $( ".uiRangeSlider" ).slider( "values", 0 ) +" - $" + $( ".uiRangeSlider" ).slider( "values", 1 ));
			
	$( ".uiMinRange" ).slider({
		range: "min",
		value: 37,
		min: 1,
		max: 700,
		slide: function( event, ui ) {
			$( "#minRangeAmount" ).val( "$" + ui.value );
		}
	});
	$( "#minRangeAmount" ).val( "$" + $( ".uiMinRange" ).slider( "value" ) );
	
	$( ".uiMaxRange" ).slider({
		range: "max",
		min: 1,
		max: 100,
		value: 20,
		slide: function( event, ui ) {
			$( "#maxRangeAmount" ).val( ui.value );
		}
	});
	$( "#maxRangeAmount" ).val( $( ".uiMaxRange" ).slider( "value" ) );	
	
	
	
	$( "#eq > span" ).each(function() {
		// read initial values from markup and remove that
		var value = parseInt( $( this ).text(), 10 );
		$( this ).empty().slider({
			value: value,
			range: "min",
			animate: true,
			orientation: "vertical"
		});
	});*/
	
	
	//===== Breadcrumbs =====//	

	$("#breadCrumb").jBreadCrumb();
	
	
	//===== Autofocus =====//	
	
	$('.autoF').focus();


	//===== Tabs =====//
		
	$.fn.simpleTabs = function(){ 
	
		//Default Action
		$(this).find(".tab_content").hide(); //Hide all content
		$(this).find("ul.tabs li:first").addClass("activeTab").show(); //Activate first tab
		$(this).find(".tab_content:first").show(); //Show first tab content
	
		//On Click Event
		$("ul.tabs li").click(function() {
			$(this).parent().parent().find("ul.tabs li").removeClass("activeTab"); //Remove any "active" class
			$(this).addClass("activeTab"); //Add "active" class to selected tab
			$(this).parent().parent().find(".tab_content").hide(); //Hide all tab content
			var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
			$(activeTab).show(); //Fade in the active content
			return false;
		});
	
	};//end function

	$("div[class^='widget']").simpleTabs(); //Run function on any div with class name of "Simple Tabs"


	//===== Placeholder for all browsers =====//
	
	$("input").each(
		function(){
			if($(this).val()=="" && $(this).attr("placeholder")!=""){
			$(this).val($(this).attr("placeholder"));
			$(this).focus(function(){
				if($(this).val()==$(this).attr("placeholder")) $(this).val("");
			});
			$(this).blur(function(){
				if($(this).val()=="") $(this).val($(this).attr("placeholder"));
			});
		}
	});
	
	
	
	
	
	
	


	//===== User nav dropdown =====//		

	$('.dd').click(function () {
		$('ul.menu_body').slideToggle(100);
	});
	
	$(document).bind('click', function(e) {
	var $clicked = $(e.target);
	if (! $clicked.parents().hasClass("dd"))
		$("ul.menu_body").hide(10);
	});

	
	
	$('.acts').click(function () {
		$('ul.actsBody').slideToggle(100);
	});
	
	
	//===== Collapsible elements management =====//
	
	$('.active').collapsible({
		defaultOpen: 'current',
		cookieName: 'nav',
		speed: 300
	});
	
	$('.exp').collapsible({
		defaultOpen: 'current',
		cookieName: 'navAct',
		cssOpen: 'active',
		cssClose: 'inactive',
		speed: 300
	});

	$('.opened').collapsible({
		defaultOpen: 'opened,toggleOpened',
		cssOpen: 'inactive',
		cssClose: 'normal',
		speed: 200
	});

	$('.closed').collapsible({
		defaultOpen: '',
		cssOpen: 'inactive',
		cssClose: 'normal',
		speed: 200
	});
	
	




	//===== Flot settings. You can place your own flot settings here =====//


	/* Lines */
	
	$(function () {
    var sin = [], cos = [];
    for (var i = 0; i < 21; i += 0.5) {
        sin.push([i, Math.sin(i)]);
        cos.push([i, Math.cos(i)]);
    }

    var plot = $.plot($(".chart"),
           [ { data: sin, label: "sin(x)"}, { data: cos, label: "cos(x)" } ], {
               series: {
                   lines: { show: true },
                   points: { show: true }
               },
               grid: { hoverable: true, clickable: true },
               yaxis: { min: -1.1, max: 1.1 },
			   xaxis: { min: 0, max: 20 }
             });

    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #000',
            padding: '2px',
			'z-index': '9999',
            'background-color': '#202020',
			'color': '#fff',
			'font-size': '11px',
            opacity: 0.8
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $(".chart").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if ($("#enableTooltip:checked").length > 0) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;
                    
                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);
                    
                    showTooltip(item.pageX, item.pageY,
                                item.series.label + " of " + x + " = " + y);
                }
            }
            else {
                $("#tooltip").remove();
                previousPoint = null;            
            }
        }
    });

    $(".chart").bind("plotclick", function (event, pos, item) {
        if (item) {
            $("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
            plot.highlight(item.series, item.datapoint);
        }
    });
});



	/* Lines with autodrowing */

	$(function () {
		// we use an inline data source in the example, usually data would
		// be fetched from a server
		var data = [], totalPoints = 200;
		function getRandomData() {
			if (data.length > 0)
				data = data.slice(1);
	
			// do a random walk
			while (data.length < totalPoints) {
				var prev = data.length > 0 ? data[data.length - 1] : 50;
				var y = prev + Math.random() * 10 - 5;
				if (y < 0)
					y = 0;
				if (y > 100)
					y = 100;
				data.push(y);
			}
	
			// zip the generated y values with the x values
			var res = [];
			for (var i = 0; i < data.length; ++i)
				res.push([i, data[i]])
			return res;
		}
	
		// setup control widget
		var updateInterval = 1000;
		$("#updateInterval").val(updateInterval).change(function () {
			var v = $(this).val();
			if (v && !isNaN(+v)) {
				updateInterval = +v;
				if (updateInterval < 1)
					updateInterval = 1;
				if (updateInterval > 2000)
					updateInterval = 2000;
				$(this).val("" + updateInterval);
			}
		});
	
		// setup plot
		var options = {
			yaxis: { min: 0, max: 100 },
			xaxis: { min: 0, max: 100 },
			colors: ["#afd8f8"],
			series: {
					   lines: { 
							lineWidth: 2, 
							fill: true,
							fillColor: { colors: [ { opacity: 0.6 }, { opacity: 0.2 } ] },
							//"#dcecf9"
							steps: false
	
						}
				   }
		};
		var plot = $.plot($(".autoUpdate"), [ getRandomData() ], options);
	
		function update() {
			plot.setData([ getRandomData() ]);
			// since the axes don't change, we don't need to call plot.setupGrid()
			plot.draw();
			
			setTimeout(update, updateInterval);
		}
	
		update();
	});



	/* Bars */

	$(function () {
    var d2 = [[0.6, 29], [2.6, 13], [4.6, 46], [6.6, 30], [8.6, 48], [10.6, 22], [12.6, 40], [14.6, 32], [16.6, 39], [18.6, 16], [20.6, 27], [22.6, 22], [24.6, 2], [26.6, 45], [28.6, 23], [30.6, 28], [32.6, 30], [34.6, 40], [36.6, 20], [38.6, 47], [40.6, 12], [42.6, 49], [44.6, 28], [46.6, 15], [48.6, 24]];
	
    var plot = $.plot($(".bars"),
           [ { data: d2} ], {
               series: {
                   bars: { 
					show: true,
					lineWidth: 0.5,
					barWidth: 0.85, 
					fill: true,
					fillColor: { colors: [ { opacity: 0.8 }, { opacity: 1 } ] },
					align: "left", 
					horizontal: false,
				   },
               },
               grid: { hoverable: true, clickable: true },
               yaxis: { min: 0, max: 50 },
			   xaxis: { min: 0, max: 50 },
             });

	});






	/* Pie charts */
	
	$(function () {
		var data = [];
		var series = Math.floor(Math.random()*10)+1;
		for( var i = 0; i<series; i++)
		{
			data[i] = { label: "Series"+(i+1), data: Math.floor(Math.random()*100)+1 }
		}
	
	$.plot($("#graph1"), data, 
	{
			series: {
				pie: { 
					show: true,
					radius: 1,
					label: {
						show: true,
						radius: 2/3,
						formatter: function(label, series){
							return '<div style="font-size:11px;text-align:center;padding:2px;color:white;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
						},
						threshold: 0.1
					}
				}
			},
			legend: {
				show: false
			},
			grid: {
				hoverable: false,
				clickable: true
			},
	});
	$("#interactive").bind("plothover", pieHover);
	$("#interactive").bind("plotclick", pieClick);
	
	$.plot($("#graph2"), data, 
	{
			series: {
				pie: { 
					show: true,
					radius:300,
					label: {
						show: true,
						formatter: function(label, series){
							return '<div style="font-size:11px;text-align:center;padding:2px;color:white;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
						},
						threshold: 0.1
					}
				}
			},
			legend: {
				show: false
			},
			grid: {
				hoverable: false,
				clickable: true
			}
	});
	$("#interactive").bind("plothover", pieHover);
	$("#interactive").bind("plotclick", pieClick);
	});
	
	function pieHover(event, pos, obj) 
	{
		if (!obj)
					return;
		percent = parseFloat(obj.series.percent).toFixed(2);
		$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
	}
	function pieClick(event, pos, obj) 
	{
		if (!obj)
					return;
		percent = parseFloat(obj.series.percent).toFixed(2);
		alert(''+obj.series.label+': '+percent+'%');
	}

	
});
