/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : harley

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2013-04-28 07:48:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `audit_answers`
-- ----------------------------
DROP TABLE IF EXISTS `audit_answers`;
CREATE TABLE `audit_answers` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`id_audit`  int(10) NULL DEFAULT NULL ,
`id_dealer`  int(10) NULL DEFAULT NULL ,
`validate`  int(1) NULL DEFAULT NULL ,
`status`  int(1) NULL DEFAULT NULL ,
`answers`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`date`  datetime NULL DEFAULT NULL ,
`group_id`  int(10) NULL DEFAULT NULL ,
`notes`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
`annual`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of audit_answers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `audit_types`
-- ----------------------------
DROP TABLE IF EXISTS `audit_types`;
CREATE TABLE `audit_types` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=7

;

-- ----------------------------
-- Records of audit_types
-- ----------------------------
BEGIN;
INSERT INTO `audit_types` VALUES ('2', 'Sales'), ('3', 'Marketing'), ('5', 'Dealer Standars'), ('6', 'Retail');
COMMIT;

-- ----------------------------
-- Table structure for `auditorias`
-- ----------------------------
DROP TABLE IF EXISTS `auditorias`;
CREATE TABLE `auditorias` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`title`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`group_id`  int(10) NULL DEFAULT NULL ,
`status`  int(1) NULL DEFAULT NULL ,
`auditor`  int(10) NULL DEFAULT NULL ,
`audit_type`  int(10) NULL DEFAULT NULL ,
`post_date`  datetime NULL DEFAULT NULL ,
`dealer`  int(10) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of auditorias
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `dealers`
-- ----------------------------
DROP TABLE IF EXISTS `dealers`;
CREATE TABLE `dealers` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`manager`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`phone`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`address`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`group_id`  int(3) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of dealers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`parent_id`  int(10) NULL DEFAULT NULL ,
`lft`  int(10) NULL DEFAULT NULL ,
`rght`  int(10) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=145

;

-- ----------------------------
-- Records of groups
-- ----------------------------
BEGIN;
INSERT INTO `groups` VALUES ('25', 'Global', null, '1', '26'), ('131', 'LATAM', '25', '2', '7'), ('132', 'EMEA', '25', '8', '13'), ('133', 'NA', '25', '14', '19'), ('134', 'AP', '25', '20', '25'), ('135', 'Mexico', '131', '3', '6'), ('136', 'Cuernavaca', '135', '4', '5'), ('137', 'EspaÃ±a', '132', '9', '12'), ('138', 'Madrid', '137', '10', '11'), ('141', 'California', '133', '15', '18'), ('142', 'Orange County', '141', '16', '17'), ('143', 'Japon', '134', '21', '24'), ('144', 'Fukushima', '143', '22', '23');
COMMIT;

-- ----------------------------
-- Table structure for `helps`
-- ----------------------------
DROP TABLE IF EXISTS `helps`;
CREATE TABLE `helps` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`question`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`answer`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=8

;

-- ----------------------------
-- Records of helps
-- ----------------------------
BEGIN;
INSERT INTO `helps` VALUES ('2', 'How are internal audits scheduled? ', 'The Internal Audit department periodically performs assessments of the College\'s operating units and control functions to identify areas of potential institutional risk. Based on these assessments and discussions with management, the Internal Auditor.'), ('4', 'Who audits the Internal Audit department?  ', 'Currently, the quality assurance review is not performed at the College. However, the Office of Internal Audit follows the Standards of the Institute of Internal Auditors (IIA). Accordingly, periodically an outside review team performs a peer review. Curr'), ('5', 'How do you help ensure quality client service? ', 'At the completion of each audit engagement, the Internal Auditor requests primary audit clients to complete and return a quality service evaluation form to identify areas for improving our service. The Internal Auditor also welcomes comments at any time. '), ('6', 'Por quÃ© no sinconizo? ', 'Hay que darle al botÃ³n sincronizar.'), ('7', 'Por quÃ© cada aÃ±o soy mÃ¡s viejo?', 'The Internal Audit department periodically performs assessments of the College\'s operating units and control functions to identify areas of potential institutional risk. Based on these assessments and discussions with management, the Internal Auditor.');
COMMIT;

-- ----------------------------
-- Table structure for `profiles`
-- ----------------------------
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
`id`  int(3) NOT NULL AUTO_INCREMENT ,
`user_id`  int(3) NULL DEFAULT NULL ,
`name`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`audit_type`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`dealer`  int(10) NULL DEFAULT NULL ,
`email`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`lang`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`company`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`note`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=4

;

-- ----------------------------
-- Records of profiles
-- ----------------------------
BEGIN;
INSERT INTO `profiles` VALUES ('3', '3', 'Yair Villar', '', null, 'servidorv@gmail.com', 'Español', 'Same', 'Este es un note de pruevas');
COMMIT;

-- ----------------------------
-- Table structure for `questions`
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`audit_id`  int(10) NULL DEFAULT NULL ,
`question`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`required`  varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' ,
`open`  varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' ,
`yes_no`  varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' ,
`group_name`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of questions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`username`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`password`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`role`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
`group_id`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`status`  int(1) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=4

;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('3', 'admin', 'dabc98ad4bb956a0cbd2ce66c49eeddeb9472c12', '5', '2013-04-05 21:04:49', '2013-04-05 21:04:49', '25', '1');
COMMIT;

-- ----------------------------
-- Auto increment value for `audit_answers`
-- ----------------------------
ALTER TABLE `audit_answers` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `audit_types`
-- ----------------------------
ALTER TABLE `audit_types` AUTO_INCREMENT=7;

-- ----------------------------
-- Auto increment value for `auditorias`
-- ----------------------------
ALTER TABLE `auditorias` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `dealers`
-- ----------------------------
ALTER TABLE `dealers` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `groups`
-- ----------------------------
ALTER TABLE `groups` AUTO_INCREMENT=145;

-- ----------------------------
-- Auto increment value for `helps`
-- ----------------------------
ALTER TABLE `helps` AUTO_INCREMENT=8;

-- ----------------------------
-- Auto increment value for `profiles`
-- ----------------------------
ALTER TABLE `profiles` AUTO_INCREMENT=4;

-- ----------------------------
-- Auto increment value for `questions`
-- ----------------------------
ALTER TABLE `questions` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `users`
-- ----------------------------
ALTER TABLE `users` AUTO_INCREMENT=4;
